# ARDUINOS for CDF19

There are 3 arduinos on the big robot `base`, `rotor` and `tower`.
There only one on the small robot `small`.

The Makefile targets are :
  - `make <arduino name>` : check the code for compilation errors
  - `make` : check all the arduinos
  - `make OUT=port up<arduino name>` : upload that arduino code to `port`

For the `tower` arduino you need the patched `ardyno` library. My fork is here :
[https://github.com/CuiCui66/ardyno.git](https://github.com/CuiCui66/ardyno.git)
