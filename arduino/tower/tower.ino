#include <DynamixelMotor.h>
#include <Servo.h>

const byte soft_rxtx = 3; // serial pin for dynamixel
const int SERVO1 = 6;
const int SERVO2 = 9;
const int SERVO3 = 10;
const int SERVO4 = 11;
const int BUTTON1 = 4;
const int BUTTON2 = 7;
const int BUTTON3 = 8;

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;


const long unsigned int baudrate = 57600;

// Dynamixel interface
SoftwareDynamixelInterface interface(soft_rxtx,soft_rxtx);

DynamixelMotor motor(interface, 1);

const byte ID = 2;

void setup()
{
  Serial.begin(57600);
  interface.begin(baudrate);
  delay(100);

  // check if we can communicate with the motor
  // if not, we turn the led on and stop here
  uint8_t status=motor.init();
  if(status!=DYN_STATUS_OK)
  {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    while(1){
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }


  motor.write(DYN_ADDRESS_LED, true);
  delay(500);
  motor.write(DYN_ADDRESS_LED, false);

  motor.write(DYN_ADDRESS_CW_LIMIT, uint16_t(455));
  motor.write(DYN_ADDRESS_CCW_LIMIT, uint16_t(570));

  motor.write(DYN_ADDRESS_CW_LIMIT, uint16_t(455));
  motor.write(DYN_ADDRESS_CCW_LIMIT, uint16_t(570));

  motor.write(DYN_ADDRESS_CW_COMP_MARGIN, uint8_t(0));
  motor.write(DYN_ADDRESS_CCW_COMP_MARGIN, uint8_t(0));
  motor.write(DYN_ADDRESS_CW_COMP_SLOPE, uint8_t(0));
  motor.write(DYN_ADDRESS_CCW_COMP_SLOPE, uint8_t(0));
  motor.write(DYN_ADDRESS_TORQUE_LIMIT, uint16_t(700));





  motor.enableTorque();

  motor.jointMode(0, 1023);
  motor.speed(512);

  pinMode(BUTTON1, INPUT);
  pinMode(BUTTON2, INPUT);
  pinMode(BUTTON3, INPUT);
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);
  servo3.attach(SERVO3);
  servo4.attach(SERVO4);

  for(char c = 0; c < 16; ++c) {
    Serial.write(c);
  }
}

void writeu16(uint16_t n){
  Serial.write((n)    &0xFF);
  Serial.write((n>>8) &0xFF);
}


uint16_t readu16(){
  uint16_t n=Serial.read();
  n += Serial.read() << 8;
  return n;
}


int speed = 512;
//change motion direction every 5 seconds
void loop()
{

  if(Serial.available() > 0 && Serial.peek() == 'i') {
    // identify the arduino
    Serial.read();
    Serial.write(ID);
  }

  if(Serial.available() >= 3 && Serial.peek() == 'g'){
    Serial.read();
    switch(Serial.read()){
      case 'p' :
        Serial.read(); //id not used
        writeu16(motor.currentPosition());
        break;
      case 's' :
        Serial.read();
        writeu16(motor.currentSpeed());
        break;
    }
  }

  uint16_t u;
  if(Serial.available() >= 5 && Serial.peek() == 't'){
    Serial.read();
    switch(Serial.read()){
      case 'p' :
        Serial.read();
        motor.goalPosition(readu16());
        break;
      case 's' :
        Serial.read();
        motor.speed(readu16());
        break;
    }
  }

  if(Serial.available() > 2 && Serial.peek() == 's') {
    Serial.read();
    byte id = Serial.read();
    unsigned char angle = Serial.read();
    if(id == 0) {
      servo1.write(angle);
    } else if(id == 1) {
      servo2.write(angle);
    } else if(id == 2) {
      servo3.write(angle);
    } else if(id == 3) {
      servo4.write(angle);
    }
  }

  if(Serial.available() > 1 && Serial.peek() == 'b') {
    Serial.read();
    byte id = Serial.read();
    if(id == 0) {
      Serial.write(digitalRead(BUTTON1) == HIGH);
    } else if(id == 1) {
      Serial.write(digitalRead(BUTTON2) == HIGH);
    } else if(id == 2) {
      Serial.write(digitalRead(BUTTON3) == HIGH);
    }
  }

  while(Serial.available() > 0 && Serial.peek() == '\n') {
    Serial.read();
  }

}
