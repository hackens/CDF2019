//The sample code for driving one way motor encoder
const byte encoder1pinA = 2;//A pin -> the interrupt pin 2
const byte encoder1pinB = 4;//B pin -> the digital pin 4
const byte encoder2pinA = 3;//A pin -> the interrupt pin 3
const byte encoder2pinB = 5;//B pin -> the digital pin 5
byte encoder1PinALast;
byte encoder2PinALast;
long int pos1;//the number of the pulses
long int pos2;//the number of the pulses
boolean direction1;//the rotation direction
boolean direction2;//the rotation direction


const int E1 = 6;
const int M1 = 7;
const int E2 = 9;
const int M2 = 8;
const int BUTTON1 = 10;
const int BUTTON2 = 11;
const int TRIG1 = 13;
const int ECHO1 = 12;
const byte ID = 0;

unsigned long getDuration(int trig, int echo) {
  long duration;
  float distance;
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  return pulseIn(echo, HIGH);
}

void setup() {
  Serial.begin(57600);//Initialize the serial port
  EncoderInit();//Initialize the module

  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(BUTTON1, INPUT);
  pinMode(BUTTON2, INPUT);
  pinMode(TRIG1, OUTPUT);
  pinMode(ECHO1, INPUT);

  for(char c = 0; c < 16; ++c) {
    Serial.write(c);
  }
}

void writeuint(unsigned long int n) {
  Serial.write((n)    &0xFF);
  Serial.write((n>>8) &0xFF);
  Serial.write((n>>16)&0xFF);
  Serial.write((n>>24)&0xFF);
}

void writeint(long int n) {
  writeuint(*((unsigned long int*)(&n)));
}

void loop()
{
  if(Serial.available() > 0 && Serial.peek() == 'i') {
    // identify the arduino
    Serial.read();
    Serial.write(ID);
  }

  if(Serial.available() > 2 && Serial.peek() == 'm') {
    // Set motor voltage
    Serial.read();
    byte byte1 = Serial.read();
    byte speed = Serial.read();
    bool motorNum = (byte1 & 0x10);
    bool dir = (byte1 & 0x1);
    digitalWrite(motorNum ? M2 : M1, dir ? LOW : HIGH);
    analogWrite (motorNum ? E2 : E1, speed);
  }

  if(Serial.available() > 1 && Serial.peek() == 'p') {
    // Get encoder value
    Serial.read();
    byte id = Serial.read();
    if(id == 0) {
      writeint(pos1);
    } else {
      writeint(pos2);
    }
  }

  if(Serial.available() > 1 && Serial.peek() == 'b') {
    Serial.read();
    byte id = Serial.read();
    if(id == 0) {
      Serial.write(digitalRead(BUTTON1) == HIGH);
    } else if(id == 1) {
      Serial.write(digitalRead(BUTTON2) == HIGH);
    }
  }

  if(Serial.available() > 1 && Serial.peek() == 'd') {
    Serial.read();
    byte id = Serial.read();
    if(id == 0) {
      writeuint(getDuration(TRIG1, ECHO1));
    }
  }

  while(Serial.available() > 0 && Serial.peek() == '\n') {
    Serial.read();
  }
}

void EncoderInit()
{
  direction1 = true;//default -> Forward
  pinMode(encoder1pinB,INPUT);
  attachInterrupt(digitalPinToInterrupt(encoder1pinA), wheelSpeed1, CHANGE);
  direction2 = true;//default -> Forward
  pinMode(encoder2pinB,INPUT);
  attachInterrupt(digitalPinToInterrupt(encoder2pinA), wheelSpeed2, CHANGE);
}

void wheelSpeed1()
{
  int Lstate = digitalRead(encoder1pinA);
  if((encoder1PinALast == LOW) && Lstate==HIGH)
  {
    int val = digitalRead(encoder1pinB);
    if(val == LOW)  direction1 = false; //Reverse
    if(val == HIGH) direction1 = true;  //Forward
  }
  encoder1PinALast = Lstate;

  if(direction1) pos1--;
  else           pos1++;
}

void wheelSpeed2()
{
  int Lstate = digitalRead(encoder2pinA);
  if((encoder2PinALast == LOW) && Lstate==HIGH)
  {
    int val = digitalRead(encoder2pinB);
    if(val == LOW)  direction2 = false; //Reverse
    if(val == HIGH) direction2 = true;  //Forward
  }
  encoder2PinALast = Lstate;

  if(direction2) pos2--;
  else           pos2++;
}
