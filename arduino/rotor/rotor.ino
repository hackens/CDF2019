#include <Servo.h>

const int TRIG1 = 12;
const int ECHO1 = 13;
const int TRIG2 = 7;
const int ECHO2 = 8;
const int SERVO1 = 6;
const int SERVO2 = 9;
const int SERVO3 = 10;
const int SERVO4 = 11;

const byte encoderA = 2;//A pin -> the interrupt pin 2
const byte encoderB = 4;//B pin -> the digital pin 4
const int E = 3;
const int M = 5;

byte encoderPinALast;
long int pos;//the number of the pulses
boolean direction;//the rotation direction

const byte ID = 1;
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;

void writeuint(unsigned long int n) {
  Serial.write((n)    &0xFF);
  Serial.write((n>>8) &0xFF);
  Serial.write((n>>16)&0xFF);
  Serial.write((n>>24)&0xFF);
}

void writeint(long int n) {
  writeuint(*((unsigned long int*)(&n)));
}

void setup() {
  Serial.begin(57600);
  pinMode(TRIG1, OUTPUT);
  pinMode(ECHO1, INPUT);
  pinMode(TRIG2, OUTPUT);
  pinMode(ECHO2, INPUT);
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);
  servo3.attach(SERVO3);
  servo4.attach(SERVO4);
  EncoderInit();
  pinMode(M, OUTPUT);
  for(char c = 0; c < 16; ++c) {
    Serial.write(c);
  }
}

unsigned long getDuration(int trig, int echo) {
  long duration;
  float distance;
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  return pulseIn(echo, HIGH);
}

void loop()
{
  if(Serial.available() > 0 && Serial.peek() == 'i') {
    Serial.read();
    Serial.write(1);
  }

  if(Serial.available() > 1 && Serial.peek() == 'd') {
    Serial.read();
    byte id = Serial.read();
    if(id == 0) {
      writeuint(getDuration(TRIG1, ECHO1));
    } else {
      writeuint(getDuration(TRIG2, ECHO2));
    }
  }

  if(Serial.available() > 2 && Serial.peek() == 's') {
    Serial.read();
    byte id = Serial.read();
    unsigned char angle = Serial.read();
    if(id == 0) {
      servo1.write(angle);
    } else if(id == 1) {
      servo2.write(angle);
    } else if(id == 2) {
      servo3.write(angle);
    } else if(id == 3) {
      servo4.write(angle);
    }
  }

  if(Serial.available() > 2 && Serial.peek() == 'm') {
    Serial.read();
    byte byte1 = Serial.read();
    byte speed = Serial.read();
    bool motorNum = (byte1 & 0x10);
    bool dir = (byte1 & 0x1);
    digitalWrite(M, dir ? LOW : HIGH);
    analogWrite (E, speed);
  }

  if(Serial.available() > 1 && Serial.peek() == 'p') {
    Serial.read();
    byte id = Serial.read();
    writeint(pos);
  }

  while(Serial.available() > 0 && Serial.peek() == '\n') {
    Serial.read();
  }
}


void EncoderInit()
{
  direction = true;//default -> Forward
  pinMode(encoderB,INPUT);
  attachInterrupt(digitalPinToInterrupt(encoderA), wheelSpeed, CHANGE);
}


void wheelSpeed()
{
  int Lstate = digitalRead(encoderA);
  if((encoderPinALast == LOW) && Lstate==HIGH)
  {
    int val = digitalRead(encoderB);
    if(val == LOW)  direction = false; //Reverse
    if(val == HIGH) direction = true;  //Forward
  }
  encoderPinALast = Lstate;

  if(direction) pos--;
  else          pos++;
}
