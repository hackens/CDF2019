# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Linux)

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER armv8-rpi3-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER armv8-rpi3-linux-gnueabihf-g++)
