# CDF19 control program

## Build options

There are two version of this program : one for the small robot and one for the big one.
The difference is made by the CMake boolean variable `BIG\_ROBOT` which in turn
will define or not a macro `BIG\_ROBOT` in the C++ code.

If you have a `aarch64-linux-gnu-gcc`, you can cross compile. By setting the CMake
variable `CMAKE\_TOOLCHAIN\_FILE` to the `raspi.cmake` file.

## Init script

The `init` script just create 6 out of source builds that are already configured :
 - `bhost` : Big robot on host compiler
 - `shost` : Small robot on host compiler
 - `braspi` : Big robot on cross compiler to raspi3
 - `sraspi` : Small robot on cross compiler to raspi3
 - `braspi1` : Big robot on cross compiler to raspi1
 - `sraspi1` : Small robot on cross compiler to raspi1

## CMake cheatsheet
 - Initialization (done by `init`) : `cmake -B folder -DVariable=value -D ...`
 - Build all folder : `cmake --build folder`
 - Build target in folder : `cmake --build folder --target target`
 - Edit options (robot version, Debug mode, ...) : `ccmake -B folder`
 
