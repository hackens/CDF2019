#include <iostream>
#include <algorithm>
#include <filesystem>
#include <unistd.h>

#include "arduino.h"
#include "units.h"
#include "calibration.h"
#include "serial.h"
#include "test.h"
#include "pid.h"
#include "geom.h"
#include "command.h"
#include "controllers.h"
#include "thread"
#include "prettyprint.hpp"

using namespace std;


//Hierarchy :

// Serial : handle Serial communications

// Protocol Controller :
// - LL motors & sensors series : direct interface to arduino commands

// Component controller :
// - motors and sensors inheriting from their LL versions

// Arduino Low-level Controllers // handle intializion of set of component using calibration
// Motors // control movement motors
// Base // control rotor engine, Basket servos, all stator sensors
// Tower // control all motors and

// Med-level controllers
// Position pos and rotation
// Baskets 1 & 2 : should be discernible // ON, OFF
// Tower : 2 controllers // polar coordinates.
// Gripper 1 & 2 : should be independent

// High level controller : TODO




// Sensors Big:
// Distance sensors : front and back
// Contact sensors : front and back
// Grip sensors : G1 & G2
//

// Sensors Small :
// Distance sensor : front
// Contact sensor : to be determined.



// Global time management

// Wifi module
// send back and forth position


#define NO_TEST              0
#define TEST_MOTORS2         1
#define TEST_ULTRASOUND      2
#define TEST_SERVO           3
#define TEST_BUTTON          4
#define TEST_MOTORS1         5
#define TEST_SELFCHECK       6
#define TEST_PID             7
#define TEST_NICESERVO       8
#define TEST_POSCONTROLLER   9
#define TEST_CMD            10
#define TEST_MEASURES_TWO   11
#define TEST_MOTORS3        12
#define TEST_HOMOLOGATION   13
#define TEST_ULTRASOUND1    14
#define TEST_DUMBFORWARD    15
#define TEST_INTEGRATION    16
#define TEST_MOTORS25       17
#define TEST_MOVING         18
#define TEST_HOMOLOGATION2  19
#define TEST_MATCH          20


#define TEST TEST_HOMOLOGATION2


int main(int argc, char** argv){

    // Serial a,b,c

    // Identify Serials

    // Motors mot(a);
    // Rotor rot (c);
    // Tower tow (b);

    // PosController pc(mot);
    // ...


    // main loop
    // update control command,
    // interact with arduino and WiFi , MT ?
    // tick time.

    Serial* serials[3] = {nullptr};
    vector<Serial*> serials_unordered;

    for(auto& p : filesystem::directory_iterator("/dev")) {
        string filename6 = p.path().filename().string().substr(0, 6);
        if(filename6 == "ttyACM" or filename6 == "ttyUSB") {
            serials_unordered.push_back(new Serial(p.path().string()));
        }
    }

    for(Serial* s : serials_unordered) {
        s->waitUntilReady();
        auto id = s->getId();
        printf("got id %d\n", id);
        assert(0 <= id && id < 3);
        serials[id] = s;
    }

    Motors mot(*serials[0]);
#ifdef BIG_ROBOT
    Rotor rot(*serials[1]);
    Tower tow(*serials[2]);
    PosController pc(mot, 5_cmps2);
#endif

#if TEST == NO_TEST
    //TODO roflmao

#elif TEST == TEST_MOTORS2

    LLDCEncoder m1(*serials[0], 0, false);
    LLDCEncoder m2(*serials[0], 1, true);
    while(true) {
        printf("%d %d\n", m1.get_ticks(), m2.get_ticks());
    }

#elif TEST == TEST_MOTORS25

    while(true) {
        cout << mot.motors[0].get_angle() << " " <<mot.motors[1].get_angle() << endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }


#elif TEST == TEST_ULTRASOUND

    LLDistSensor ds(*serials[1], 1);
    const double TEMP = 25;
    const speed SOUND_SPEED = speed::meters_per_second(20.05*sqrt(273.15 + TEMP));
    while(true) {
        printf("%lf\n", (SOUND_SPEED*(ds.poll_time()/2)).cm());
    }

#elif TEST == TEST_ULTRASOUND1

    while(true) {
        cout << rot.ds[0].get() << " " << rot.ds[1].get() <<endl;
        if(rot.ds[0].get() < 30_cm or rot.ds[1].get() < 30_cm) {
            cout << "stopped" << endl;
        }

        sleep(1);
    }


#elif TEST == TEST_SERVO

    LLBasicServo bs(*serials[1], 3);
    while(true) {
        printf("toto\n");
        bs.set_pos(0);
        sleep(1);
        bs.set_pos(M_PI/2);
        sleep(1);
        bs.set_pos(M_PI);
        sleep(1);
        bs.set_pos(M_PI/2);
        sleep(1);
    }

#elif TEST == TEST_BUTTON

    LLButton b(*serials[1], 1);
    while(true) {
        printf("%d\n", int(b.pressed()));
    }

#elif TEST == TEST_MOTORS1

    LLDCEncoder m(*serials[1], 0, false);
    m.set_voltage(255, false);
    while(true) {
        printf("%d\n", m.get_ticks());
    }

#elif TEST == TEST_SELFCHECK

    selfcheck(serials);

#elif TEST == TEST_PID

    LLDCEncoder m1(*serials[0], 0, false);

    int goal = 100000;
    //PID pid(0.7, 0.01, 0.1);
    PID pid(3, 0.01, 0.1);

    while(true) {
        int enc = m1.get_ticks();
        double d = pid.update(goal-enc);
        u8 voltage = u8(min(fabs(d)+30, double(255)));
        printf("                                   \r");
        printf("  %d %.03lf %d\r", enc, d, int(voltage));
        fflush(stdout);
        m1.set_voltage(voltage, d > 0);
        usleep(10000);
    }


#elif TEST == TEST_NICESERVO
    cout << "hey" << endl;
    LLNiceServo m(*serials[2], 0);
    cout << "hey2" << endl;
    m.set_spd(2_rpm);
    m.set_pos(135_deg);

    while(true) {
        cout << "pos : " << m.pos() << endl;
        sleep(1);
    }
#elif TEST == TEST_POSCONTROLLER
    PosController pc(mot, 1_mps2);
    PosController::PosTrajectory pt(pc, [](duration t) { return Vec2p{0_m, 0_m}; });

#elif TEST == TEST_CMD
    Control ctrl;
    ctrl.serials = serials;
    ctrl.mot = mot;
    #ifdef BIG_ROBOT
      ctrl.pc = pc;
      ctrl.rot = rot;
      ctrl.tow = tow;
    #endif
    if (argc != 0)
        cmd(argc, argv, ctrl);
    else
        inter(ctrl);

#elif TEST == TEST_MEASURES_TWO
    while(true) {
        mot.update();
        printf("%lf %lf\n", remove_unit(mot.motors[0].spd()), remove_unit(mot.motors[1].spd()));
    }

#elif TEST == TEST_MOTORS3

    mot.motors[0].set_spd(-300_degps);
    mot.motors[1].set_spd(-300_degps);

    while(true){
        mot.motors[0].update();
        mot.motors[1].update();
        cout << mot.motors[0].spd() << " " << mot.motors[1].spd() <<endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }

#elif TEST == TEST_HOMOLOGATION2


    while(tow.gripped[1].pressed());

    pc.doNow<PosController::PosTrajectory>([](duration t) {
        // if (get_sec(t) < 10) {
        //     return pair(Vec2p{5_cmps * t, 0_m},false);
        // }
        // else {
        return pair(Vec2p{70_cm, 0_cm}, false);
        // }
    });

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().p - Vec2p{70_cm, 0_cm}).norm() < 10_cm) break;
        if(rot.ds[0].get() < 20_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }

    }
    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

    cout << "2" << endl;

    pc.doNow<PosController::PosTrajectory>(
        [](duration t) {
            // if (get_sec(t) < 10) {
            //     return pair(Vec2p{5_cmps * t, 0_m},false);
            // }
            // else {
            return pair(Vec2p{10_m * cos(35_deg), 10_m * sin(35_deg)}, false);
            // }
        },
        true);

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().t - 35_deg).abs() < 5_deg) break;
    }

    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

    cout <<"3" << endl;

    time_point start = now();

    pc.doNow<PosController::PosTrajectory>([](duration t) {
        // if (get_sec(t) < 10) {
        //     return pair(Vec2p{5_cmps * t, 0_m},false);
        // }
        // else {
        return pair(Vec2p{209_mm, -344_mm}, false);
        // }
    });

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().p -Vec2p{209_mm,-344_mm}).norm() < 10_cm) break;
        if(rot.ds[1].get() < 20_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }
        if(now() - start > fsec(20)) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "time out" << endl;
            return 0;
        }
    }
    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

#elif TEST == TEST_MATCH


    while(tow.gripped[1].pressed());

    pc.doNow<PosController::PosTrajectory>([](duration t) {
        // if (get_sec(t) < 10) {
        //     return pair(Vec2p{5_cmps * t, 0_m},false);
        // }
        // else {
        return pair(Vec2p{70_cm, 0_cm}, false);
        // }
    });

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().p - Vec2p{70_cm, 0_cm}).norm() < 10_cm) break;
        if(rot.ds[0].get() < 30_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }

    }
    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

    cout << "2" << endl;

    pc.doNow<PosController::PosTrajectory>(
        [](duration t) {
            // if (get_sec(t) < 10) {
            //     return pair(Vec2p{5_cmps * t, 0_m},false);
            // }
            // else {
            return pair(Vec2p{10_m * cos(35_deg), 10_m * sin(35_deg)}, false);
            // }
        },
        true);

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().t - 35_deg).abs() < 5_deg) break;
    }

    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

    cout <<"3" << endl;

    time_point start = now();

    pc.doNow<PosController::PosTrajectory>([](duration t) {
        // if (get_sec(t) < 10) {
        //     return pair(Vec2p{5_cmps * t, 0_m},false);
        // }
        // else {
        return pair(Vec2p{209_mm, -344_mm}, false);
        // }
    });

    while (true) {
        pc.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if ((pc.getPos().p -Vec2p{209_mm,-344_mm}).norm() < 10_cm) break;
        if(rot.ds[1].get() < 30_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }
        if(now() - start > fsec(20)) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "time out" << endl;
            return 0;
        }
    }
    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);




#elif TEST == TEST_INTEGRATION

    int i = 0;

    cout << mot.motors[0].rev << " rmain " << mot.motors[1].rev<<endl;

    while(true){
        ++i;
        pc.update();

        cout << pc.getPos().p << " " << pc.getPos().t << endl;
        cout << mot.motors[0].get_angle() << " " <<mot.motors[1].get_angle() << endl;
        // mot.motors[1].update();
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }

#elif TEST == TEST_MOVING

    pc.doNow<PosController::PosTrajectory>([](duration t) {
        if (get_sec(t) < 30) {
            return pair(Vec2p{1_m * sin(M_PI / 2 * get_sec(t) / 30),
                              1_m * cos(M_PI / 2 * get_sec(t) / 30) - 1_m},
                        false);
        }
        else {
            return pair(Vec2p{1_m,-1_m}, true);
        }
    });

    while (true) {
        pc.update();

        cout << "Voltages : " << mot.motors[0].voltage << " "
             << mot.motors[0].voltage << endl;
        cout << "Speeds : " << mot.motors[0].spd() << " " << mot.motors[1].spd()
             << endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }



#elif TEST == TEST_HOMOLOGATION

#ifdef BIG_ROBOT

    double t1, t2;
    t1 = 1.1;
    t2 = 1.3;

    while(tow.gripped[1].pressed());


    mot.motors[0].set_voltage(120,true);
    mot.motors[1].set_voltage(100,false);
    angle st = mot.motors[0].get_angle();

    while((mot.motors[0].get_angle() - st).abs() < t1 * 1_turn){
        if(rot.ds[0].get() < 20_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }
    }



    // cout << "1" << endl;
    // mot.motors[0].set_voltage(100,true);
    // mot.motors[1].set_voltage(100,false);

    // start = now();
    // while(get_sec(now() - start) < 1){
    //     if(rot.ds[0].get() < 10_cm or rot.ds[1].get() < 10_cm) {
    //         mot.motors[0].set_voltage(0,false);
    //         mot.motors[1].set_voltage(0,true);
    //         cout << "stopped" << endl;
    //         return 0;
    //     }
    // }


    cout << "2" << endl;
    mot.motors[0].set_voltage(140,false);
    mot.motors[1].set_voltage(100,true);

    st = mot.motors[0].get_angle();
    while((mot.motors[0].get_angle() - st).abs() < t2 * 1_turn){
        if(rot.ds[1].get() < 20_cm) {
            mot.motors[0].set_voltage(0,false);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }
    }
    mot.motors[0].set_voltage(0,false);
    mot.motors[1].set_voltage(0,true);

#else

    while(mot.starter.pressed());

    mot.motors[0].set_voltage(100, false);
    mot.motors[1].set_voltage(100, true);

    while(fabs(mot.motors[0].get_angle()) < 10_rad) {
        cout << mot.motors[0].get_angle() << endl;

        if(mot.ds.get() < 30_cm) {
            mot.motors[0].set_voltage(0,true);
            mot.motors[1].set_voltage(0,true);
            cout << "stopped" << endl;
            return 0;
        }
    }
    mot.motors[0].set_voltage(0,true);
    mot.motors[1].set_voltage(0,true);
#endif

#elif TEST == TEST_POSARM

    ArmController armcontrol(rot, tow);
    Vec2p base = Vec2p::polar(26_cm, 45_deg); 
    Vec3p dest = base.x * X + base.y * Y + 40_cm * Z;
    armcontrol.set_goal(0, dest);
    Vec3p result = armcontrol.pos(0);
    std::cout << "Aimed for " << dest << " and reached " << result << std::endl;







#else

#error "error: no test selected"

#endif

    return 0;
}
