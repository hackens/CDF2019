#pragma once

#include "motors.h"
#include "sensors.h"

// Convention : Interior is the wall part, Exterior is the basket part;
// Convention : front is when Interior is to the right

struct Motors{
    enum Motor{Int,Ext};
    enum Side{Front, Back};
    DCEncoder motors[2];
    Button starter;
#ifdef BIG_ROBOT
    Button contact[2];
#else
    Button bau;
    DistSensor ds;
#endif

    Motors(Serial& s);
    void update();

};

#ifdef BIG_ROBOT

struct Rotor{
    enum Side{Front, Back};
    enum BasketHeight{Down, Up};
    DCEncoder rotor;
    BasicServo baskets[2][2];
    DistSensor ds[2];

    Rotor(Serial& s);
    void update();
};

struct Tower{
    NiceServo arm;
    BasicServo wrist[2];
    BasicServo grippers[2];
    Button gripped[2];
    Tower(Serial& s);
    void update();
};

#endif
