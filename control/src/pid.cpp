#include "pid.h"
#include <stdio.h>

double PID::update(double error) {
    time_point time = now();
    double dt = get_sec(time-last);
    if(int_err*error < 0) {
        int_err = 0;
    }
    int_err += error*dt;
    double der_err = (first ? 0 : (error-last_error)/dt);
    first = false;
    // printf("\n%lf %lf %lf\n", error, int_err, der_err);

    last = time;
    last_error = error;

    return kp*error + ki*int_err +kd*der_err;
}

