#pragma once

#include <iostream>
#include <cmath>
#include "types.h"
#include "time.h"

template<typename T>
struct unit_trait{
    template<typename = std::enable_if<std::is_convertible<T,double>::value>>
    static constexpr double remove_unit(T t){
        return t;
    }

    template<typename = std::enable_if<std::is_convertible<double,T>::value>>
    static constexpr T add_unit(double t){
        return t;
    }
};

template<typename T>
constexpr double remove_unit(T t){
    return unit_trait<T>::remove_unit(t);
}
template<typename T>
constexpr T add_unit(double t){
    return unit_trait<T>::add_unit(t);
}

template<>
struct unit_trait<duration>{
    static constexpr double remove_unit(duration t){
        return get_sec(t);
    }
    static constexpr duration add_unit(double t){
        return std::chrono::duration_cast<duration>(fsec(t));
    }
};

#define OPTIMES(T1, T2, T3)                                         \
    constexpr T1 operator*(T2 t, T3 u){                             \
        return add_unit<T1>(remove_unit<T2>(t)*remove_unit<T3>(u)); \
    }

#define OPDIV(T1, T2, T3)                                           \
    constexpr T1 operator/(T2 t, T3 u){                             \
        return add_unit<T1>(remove_unit<T2>(t)/remove_unit<T3>(u)); \
    }

// T1 = T2 * T3
#define OPREL(T1, T2, T3)                       \
    OPTIMES(T1,T2,T3)                           \
    OPTIMES(T1,T3,T2)                           \
    OPDIV(T2, T1, T3)                           \
    OPDIV(T3, T1, T2)

// UNIT(dist, _cm, 0.01)
#define UNIT(T, suff, coeff) \
    constexpr T operator"" suff(long double d) {  \
        return add_unit<T>(d * (coeff));} \
    constexpr T operator"" suff(long long unsigned d) { \
        return add_unit<T>(d * double(coeff));}


#define COMMON(T, attr)                                             \
    constexpr T& operator+=(T d) {attr += d.attr;return *this;}     \
    constexpr T& operator-=(T d) {attr -= d.attr;return *this;}     \
    constexpr T& operator*=(double d) {attr *= d;return *this;}     \
    constexpr T& operator/=(double d) {attr /= d;return *this;}     \
                                                                    \
    constexpr bool operator==(const T d) const {return attr == d.attr;}    \
    constexpr bool operator!=(const T d) const {return attr != d.attr;}    \
    constexpr bool operator<(const T d) const {return attr < d.attr;}      \
    constexpr bool operator>(const T d) const {return attr > d.attr;}      \
    constexpr bool operator<=(const T d) const {return attr <= d.attr;}    \
    constexpr bool operator>=(const T d) const {return attr >= d.attr;}    \
                                                                    \
    constexpr friend T operator*(double a, T b) {return b*=a;}      \
    constexpr friend T operator*(T a, double b) {return a*=b;}      \
    constexpr friend T operator+(T a, T b) {return a+=b;}           \
    constexpr friend T operator-(T a, T b) {return a-=b;}           \
    constexpr friend T operator-(T a) {return T(-a.attr);}          \
    constexpr friend T operator/(T a, double b) {return a /= b;}    \
    constexpr friend T operator/(T a, int b) {return a /= b;}       \
                                                                    \
    constexpr T abs(){return T(std::abs(attr));}                       \






//                    _
//   __ _ _ __   __ _| | ___
//  / _` | '_ \ / _` | |/ _ \
// | (_| | | | | (_| | |  __/
//  \__,_|_| |_|\__, |_|\___|
//              |___/

/// \brief Encapsulate an angle with conversion and all.
class angle{
    double rad;

public:
    // implict constructor from double
    constexpr angle() :rad(0){}
    constexpr angle(double rads) : rad(rads) {}

    constexpr double radians() const noexcept {return rad;}
    constexpr double degrees() const noexcept {return rad*180/M_PI;}
    constexpr double turn()const noexcept {return rad/2/M_PI;}

    COMMON(angle,rad)

    // implict cast to double because angle don't really have units.
    operator double (){return rad;}

    // human output in degrees
    friend inline std::ostream& operator<<(std::ostream& out, angle a){
        return out << a.degrees() << "°";
    }
};

UNIT(angle, _rad,1)
UNIT(angle, _deg, M_PI/180)
UNIT(angle, _turn, 2 * M_PI)


//                                            _
//   __ _ _ __   __ _ ___ _ __   ___  ___  __| |
//  / _` | '_ \ / _` / __| '_ \ / _ \/ _ \/ _` |
// | (_| | | | | (_| \__ \ |_) |  __/  __/ (_| |
//  \__,_|_| |_|\__, |___/ .__/ \___|\___|\__,_|
//              |___/    |_|

class angspeed {
    double rps;
    constexpr angspeed(double rpss) : rps(rpss) {}

public:
    constexpr angspeed() :rps(0){}
    constexpr static angspeed rad_per_second(double rps){return angspeed(rps);}

    constexpr double radps() const noexcept {return rps;}
    constexpr double degps() const noexcept {return rps/M_PI*180;}
    constexpr double rpm()   const noexcept {return rps/2./M_PI*60.;}

    COMMON(angspeed,rps)

    friend inline std::ostream& operator<<(std::ostream& out, angspeed as){
        return out << as.degps() << "°/s";
    }

};

template<>
struct unit_trait<angspeed>{
    static constexpr double remove_unit(angspeed t){
        return t.radps();
    }
    static constexpr angspeed add_unit(double t){
        return angspeed::rad_per_second(t);
    }
};

UNIT(angspeed, _radps, 1)
UNIT(angspeed, _degps, M_PI/180)
UNIT(angspeed, _rpm, 2*M_PI/60)

OPDIV(double, angspeed, angspeed)
OPREL(angle, angspeed, duration)

//      _ _     _
//   __| (_)___| |_
//  / _` | / __| __|
// | (_| | \__ \ |_
//  \__,_|_|___/\__|


/// \brief Encapsulate a dist with conversion and all.
class dist {
    double meter;
    constexpr dist(double meters) : meter(meters) {}

  public:
    constexpr dist() : meter(0){}
    constexpr static dist meters(double m){return dist(m);}

    constexpr double m() const noexcept {return meter;}
    constexpr double mm() const noexcept {return meter*1000;}
    constexpr double cm() const noexcept {return meter*100;}

    COMMON(dist, meter)

    friend inline std::ostream& operator<<(std::ostream& out, dist d){
        if(std::abs(d.meter) >= 1.0) return out << d.meter << "m";
        else return out << d.cm() << "cm";
    }
};

template<>
struct unit_trait<dist>{
    static constexpr double remove_unit(dist t){
        return t.m();
    }
    static constexpr dist add_unit(double t){
        return dist::meters(t);
    }
};

UNIT(dist, _m, 1)
UNIT(dist, _cm, 0.01)
UNIT(dist, _mm, 0.001)


OPDIV(angle, dist, dist)


//  ____                      _
// / ___| _ __   ___  ___  __| |
// \___ \| '_ \ / _ \/ _ \/ _` |
//  ___) | |_) |  __/  __/ (_| |
// |____/| .__/ \___|\___|\__,_|
//       |_|

/// \brief Encapsulate a speed with conversion and all.
class speed {
    double metps;
    constexpr speed(double metpss) : metps(metpss) {}

public:
    constexpr speed() : metps(0){}
    constexpr static speed meters_per_second(double mps){return speed(mps);}

    constexpr double mps() const noexcept {return metps;}
    constexpr double cmps() const noexcept {return metps*100;}
    constexpr double mmps() const noexcept {return metps*1000;}
    constexpr double kmph() const noexcept {return metps*3.6;}

    COMMON(speed, metps)

    friend inline std::ostream& operator<<(std::ostream& out, speed d){
        if(std::abs(d.metps) >= 1.0){
            out << d.metps << "m/s";
        }
        else {
            out << d.cmps() << "cm/s";
        }
        return out;
    }

};


template<>
struct unit_trait<speed>{
    static constexpr double remove_unit(speed t){
        return t.mps();
    }
    static constexpr speed add_unit(double t){
        return speed::meters_per_second(t);
    }
};

UNIT(speed, _mps,1)
UNIT(speed, _cmps,0.01)
UNIT(speed, _mmps,0.001)
UNIT(speed, _kmph,1./3.6)


OPDIV(double, speed, speed)
OPREL(dist, speed, duration)
OPREL(speed, dist, angspeed)





//     _                _                _   _
//    / \   ___ ___ ___| | ___ _ __ __ _| |_(_) ___  _ __
//   / _ \ / __/ __/ _ \ |/ _ \ '__/ _` | __| |/ _ \| '_ \
//  / ___ \ (_| (_|  __/ |  __/ | | (_| | |_| | (_) | | | |
// /_/   \_\___\___\___|_|\___|_|  \__,_|\__|_|\___/|_| |_|


/// \brief Encapsulate a acceleration with conversion and all.
class acceleration {
    double metps2;
    constexpr acceleration(double metps2s) : metps2(metps2s) {}

public:
    constexpr acceleration() : metps2(0){}
    constexpr static acceleration meters_per_second2(double mps2){return acceleration(mps2);}

    constexpr double mps2() const noexcept {return metps2;}
    constexpr double cmps2() const noexcept {return metps2*100;}
    constexpr double mmps2() const noexcept {return metps2*1000;}

    COMMON(acceleration,metps2)


    friend inline std::ostream& operator<<(std::ostream& out, acceleration d){
        if(std::abs(d.metps2) >= 1.0){
            out << d.metps2 << "m/s²";
        }
        else {
            out << d.cmps2() << "cm/s²";
        }
        return out;
    }

};

template<>
struct unit_trait<acceleration>{
    static constexpr double remove_unit(acceleration t){
        return t.mps2();
    }
    static constexpr acceleration add_unit(double t){
        return acceleration::meters_per_second2(t);
    }
};

UNIT(acceleration, _mps2, 1)
UNIT(acceleration, _cmps2, 0.01)
UNIT(acceleration, _mmps2, 0.001)

OPDIV(double, acceleration, acceleration)
OPREL(speed, acceleration, duration)
