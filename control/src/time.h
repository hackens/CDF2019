#pragma once

#include <chrono>

/// Here to change the clock
using clk = std::chrono::high_resolution_clock;

using duration = clk::duration;
using time_point = clk::time_point;
using fsec = std::chrono::duration<double>;

inline auto now(){return clk::now();}

constexpr inline double get_sec(duration d){return fsec(d).count();}
