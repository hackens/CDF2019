#include "motors.h"

using namespace std;

void BasicServo::set_pos(angle pos) {
    target = pos;
    LLBasicServo::set_pos(pos-origin);
}

//  _   _ _          ____
// | \ | (_) ___ ___/ ___|  ___ _ ____   _____
// |  \| | |/ __/ _ \___ \ / _ \ '__\ \ / / _ \
// | |\  | | (_|  __/___) |  __/ |   \ V / (_) |
// |_| \_|_|\___\___|____/ \___|_|    \_/ \___/


void NiceServo::update() {
    position = LLNiceServo::pos() - origin;
    speed = LLNiceServo::spd();
}

void NiceServo::set_spd(angspeed spd) {
    LLNiceServo::set_spd(spd);
    target_speed = spd;
}


//  ____   ____ _____                     _
// |  _ \ / ___| ____|_ __   ___ ___   __| | ___ _ __
// | | | | |   |  _| | '_ \ / __/ _ \ / _` |/ _ \ '__|
// | |_| | |___| |___| | | | (_| (_) | (_| |  __/ |
// |____/ \____|_____|_| |_|\___\___/ \__,_|\___|_|


static int voltageToIndex(std::pair<u8, bool> p) {
    return p.first + 256*p.second;
}

static std::pair<u8, bool> indexToVoltage(int ind) {
    return std::make_pair(ind%256, ind >= 256);
}

std::pair<u8, bool> STV_TwoMotors::getVoltage(double speed) {
    int j = voltageToIndex(other.last_voltage());
    int besti = 0;
    for(int i = 0; i < 512; ++i) {
        if(fabs(speed-vals[i][j]) < fabs(speed-vals[besti][j])) {
            besti = i;
        }
    }
    return indexToVoltage(besti);
}

void STV_TwoMotors::read(const std::string& file) {
    FILE* f = fopen(file.c_str(), "r");
    assert(f != nullptr);

    for(int i = 0; i < 512; ++i) {
        for(int j = 0; j < 512; ++j) {
            fscanf(f, "%lf", &vals[i][j]);
            measured[i][j] = vals[i][j] >= 0;
        }
    }

    //Important assertions for the correctness of the interpolation algorithm
    assert(measured[0][0]);
    assert(measured[511][0]);
    assert(measured[0][511]);
    assert(measured[511][511]);

    //Do the interpolation
    for(int i = 0; i < 512; i += 511) {
        int prevj = 0;
        int nextj = 0;
        for(int j = 0; j < 512; ++j) {
            if(measured[i][j]) {
                prevj = j;
                continue;
            }
            if(nextj <= j) {
                for(nextj = j; !measured[i][nextj]; ++nextj);
            }

            double lambda = (j-prevj)/(nextj-prevj);
            vals[i][j] = (1-lambda)*vals[i][prevj] + lambda*vals[i][nextj];
        }
    }

    //Copy-pasted...
    for(int j = 0; j < 512; ++j) {
        int previ = 0;
        int nexti = 0;
        for(int i = 0; i < 512; ++i) {
            if(i == 0 || i == 511 || measured[i][j]) {
                previ = i;
                continue;
            }
            if(nexti <= i) {
                for(nexti = i; !measured[nexti][j]; ++nexti);
            }

            double lambda = (i-previ)/(nexti-previ);
            vals[i][j] = (1-lambda)*vals[previ][j] + lambda*vals[nexti][j];
        }
    }
    fclose(f);
}

void STV_TwoMotors::write(const std::string& file) {
    FILE* f = fopen(file.c_str(), "w");
    for(int i = 0; i < 512; ++i) {
        for(int j = 0; j < 512; ++j) {
            fprintf(f, "%.10lf ", measured[i][j] ? vals[i][j] : -1);
        }
        fprintf(f, "\n");
    }
    fclose(f);
}

void STV_TwoMotors::set_measure(std::pair<u8, bool> vme, std::pair<u8, bool> voth, double speed) {
    int i = voltageToIndex(vme);
    int j = voltageToIndex(voth);
    measured[i][j] = true;
    vals[i][j] = speed;
}




void DCEncoder::push_step(i32 step, duration tick_duration) {
    // cout << "pushing " << step - last_tick_values[0] << " tick during " << get_sec(tick_duration) << endl;
    for(size_t i = LAST_SIZE - 1; i > 0; --i) {
        last_tick_values[i] = last_tick_values[i-1];
        last_times[i] = last_times[i-1];
    }

    last_times[0] = tick_duration;
    last_tick_values[0] = step;
}

void DCEncoder::update() {
    time_point cur_poll = now();
    if(cur_poll - last_poll < dt) return;
    push_step(get_ticks(), cur_poll-last_poll);
    last_poll = cur_poll;

    const double dead = 30;

    if(remove_unit(target_speed)*voltage < 0) voltage = 0;


    voltage += remove_unit(dt) * stv.update(remove_unit(target_speed -spd()));
    // cout << "voltage " << voltage << endl;
    // cout << "speed " << spd() << endl;
    if(voltage > 255 -dead) voltage = 255 -dead;
    if(voltage < -255 +dead) voltage = -255 +dead;
    set_voltage(abs(voltage)+dead,voltage < 0);
}

angle DCEncoder::get_angle() {
    return get_ticks()*angle_per_tick*ratio;
}

angspeed DCEncoder::spd() const {
    const duration dt = 100ms;
    duration duration_sum = duration::zero();
    size_t lasti = 0;
    for(size_t i = 0; i < LAST_SIZE-1; ++i) {
        duration_sum += last_times[i];
        lasti = i;
        if(duration_sum > dt) {
            break;
        }
    }

    return (last_tick_values[0] - last_tick_values[lasti + 1]) *
           angle_per_tick * ratio / duration_sum;
}

void DCEncoder::set_spd(angspeed speed) {
    target_speed = speed;
}
