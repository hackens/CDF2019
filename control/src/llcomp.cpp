#include "llcomp.h"
#include "serial.h"

using namespace std;

// All arduino protocols must be here
// p: position (cc with encoder)
// m: motor (cc with encoder)
// d: duration (ultra sound distance sensor)
// b: button
// s: servo
// g: get speed/position of nice servo
// t: seT speed/position of nice sevo ("s" was already used :( )

void LLBasicServo::set_pos(angle ang) {
    serial.put("s", char(id), (unsigned char)(round(ang.degrees())));
}

void LLNiceServo::set_pos(angle ang) {
    serial.put("tp", char(id), u16((ang + 0.1818_deg) / 0.2929_deg));
}


void LLNiceServo::set_spd(angspeed ang) {
    serial.put("ts", char(id), u16(ang / 0.111_rpm));
}

angle LLNiceServo::pos() {
    serial.put("gp", char(id));
    return double(serial.get<u16>()) * 0.2929_deg;
}

angspeed LLNiceServo::spd() {
    serial.put("gs", char(id));
    return double(serial.get<u16>()) * 0.111_rpm;
}



i32 LLDCEncoder::get_ticks() {
    serial.put("p", char(id));
    return (rev ? -1 : 1) * serial.get<i32>();
}

void LLDCEncoder::set_voltage(u8 val, bool dir) {
    _last_voltage = std::make_pair(val, dir);
    if(rev) {
        dir = !dir;
    }
    serial.put("m", u8((id << 4 ) | dir), val);
}



duration LLDistSensor::poll_time() {
    serial.put("d", char(id));
    return std::chrono::microseconds(serial.get<u32>());
}



bool LLButton::pressed() {
    serial.put("b", char(id));
    return serial.get<char>() != 0;
}
