#pragma once

#include <string>
#include <fstream>
#include <cassert>
#include <array>

#include "types.h"
#include "units.h"
#include "llcomp.h"
#include "calibration.h"
#include "pid.h"

/// On Basic Servo there is no feedback
class BasicServo : public LLBasicServo{
    const angle origin;
    const duration expected_anwser_time;
    time_point last_update;
    angle target;
public:
    BasicServo(Serial& serial, u8 id, angle norigin, duration eat):
        LLBasicServo(serial,id),origin(norigin),expected_anwser_time(eat){}
    bool is_done() const {return (now() - last_update) > expected_anwser_time;}
    void set_pos(angle pos);
    angle pos(){return target;}
};

/// NiceServo
class NiceServo : public LLNiceServo{
    angle origin; // can be determined as initial position
    angle position = 0;
    angspeed speed = 0_radps;
    angspeed target_speed = 0_radps;

public:
    NiceServo(Serial& serial, u8 id)
        : LLNiceServo(serial,id), origin(LLNiceServo::pos()) {}
    void update();
    angle pos() const {return position;}
    angspeed spd() const {return speed;}
    void set_spd(angspeed spd);
    // distance to targeted speed, it it grows to much, acceleration must lower.
    angspeed follow() const {return target_speed - speed;}
};

class SpeedToVoltage {
    public:
        virtual std::pair<u8, bool> getVoltage(double speed) = 0;
        virtual void read(const std::string& file) = 0;
        virtual void write(const std::string& file) = 0;
        virtual ~SpeedToVoltage() {}
};

class DCEncoder;

class STV_TwoMotors : public SpeedToVoltage {
    public:
        virtual std::pair<u8, bool> getVoltage(double speed);
        virtual void read(const std::string& file);
        virtual void write(const std::string& file);
        void set_measure(std::pair<u8, bool> vme, std::pair<u8, bool> voth, double speed);
    private:
        std::array<std::array<double, 512>, 512> vals;
        std::array<std::array<double, 512>, 512> measured;
        DCEncoder& other;
};

constexpr double ku = 10, tu = 8;

/// DC motor with encoder
class DCEncoder : public LLDCEncoder{
    static constexpr size_t LAST_SIZE = 10;
    const double ratio;
    time_point last_poll;
    i32 last_tick_values[LAST_SIZE]={0};
    duration last_times[LAST_SIZE]={}; //TODO ça s'initialise à 0 automatiquement ?
    angspeed target_speed = 0_rpm;
    time_point last_voltage_update;
    const duration dt = std::chrono::milliseconds(20);
    void push_step(i32 step, duration tick_duration);
    PID stv;
public :
    double voltage = 0;
public:
    DCEncoder(Serial& serial, int id, bool rev, double ratio = 1)
        : LLDCEncoder(serial,id,rev), ratio(ratio), stv(30,1,20){}
    void update();
    angle get_angle();
    angspeed spd() const;
    /// All the magic happens here.
    void set_spd(angspeed speed);
};
