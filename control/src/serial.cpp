#include "serial.h"

#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>

Serial::Serial(const std::string& filename) {
    struct termios toptions;
    _fd = open(filename.c_str(), O_RDWR | O_NOCTTY);
    if (_fd == -1)  {
        perror("init_serialport: Unable to open port ");
        exit(-1);
    }
    if (tcgetattr(_fd, &toptions) < 0) {
        perror("init_serialport: Couldn't get term attributes");
        exit(-1);
    }
    speed_t brate = B57600;
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);
    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;
    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl
    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw
    // no conversion \r -> \n
    toptions.c_iflag &= ~ICRNL;
    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 20;
    if( tcsetattr(_fd, TCSANOW, &toptions) < 0) {
        perror("init_serialport: Couldn't set term attributes");
        exit(-1);
    }
}

Serial::~Serial() {
    if(_fd >= 0) {
        close(_fd);
    }
}

void Serial::waitUntilReady() {
    //wait for the arduino to reboot
    char cur = 0;
    while(true) {
        if(cur == 16) {
            return;
        }
        char c = get<char>();
        if(c == cur) {
            ++cur;
        } else {
            if(c == 0) {
                cur = 1;
            } else {
                cur = 0;
            }
        }
    }
}

void Serial::write(const void* buf, size_t nbyte) {
    const char* cbuf = (const char*)buf;
    while(nbyte > 0) {
        size_t sz = ::write(_fd, cbuf, nbyte);
        nbyte -= sz;
        cbuf += sz;
    }
}

void Serial::read(void* buf, size_t nbyte) {
    char* cbuf = (char*)buf;
    while(nbyte > 0) {
        size_t sz = ::read(_fd, cbuf, nbyte);
        nbyte -= sz;
        cbuf += sz;
    }
}

char Serial::getId() {
    put("i");
    return get<char>();
}

