#pragma once
#include "time.h"

class PID {
    public:
        PID(double kp, double ki, double kd) : kp(kp), ki(ki), kd(kd), last(now()) {}
        double update(double error);
    private:
        double kp, ki, kd;
        time_point last;
        double last_error = 0;
        double int_err = 0;
        bool first =true;
};
