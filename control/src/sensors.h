#pragma once


#include <string>
#include <fstream>
#include <cassert>

#include "types.h"
#include "units.h"

#include "llcomp.h"
#include "calibration.h"



using Button=LLButton;

class DistSensor : private LLDistSensor{
    using LLDistSensor::LLDistSensor;
public:
    dist get();
};
