#pragma once

#include "arduino.h"
#include "geom.h"
#include "pid.h"
#include <memory>
#include <type_traits>
#include <functional>

//  ____            ____            _             _ _
// |  _ \ ___  ___ / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __
// | |_) / _ \/ __| |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__|
// |  __/ (_) \__ \ |__| (_) | | | | |_| | | (_) | | |  __/ |
// |_|   \___/|___/\____\___/|_| |_|\__|_|  \___/|_|_|\___|_|


class PosController {
public:
    class Action {
    protected:
        PosController& pc;

    public:
        Action(PosController& pc) : pc(pc) {}
        /// update the control based on local information
        virtual void tick() = 0;
        /// Tell if the action is finished
        virtual bool end() = 0;
        virtual ~Action() {}
    };

    class DumbForward : public Action {
        enum Phase { Accelerating, Stable, Decelerating } phase = Accelerating;
        dist distance;
        double dir;
        dist done = 0_m;
        speed top_speed;
        dist ramp_len;
        Vec2p last_pos;
        time_point start_phase = now();
        PID pid;

    public:
        DumbForward(PosController& pc, dist distance, bool dir = true,
                    speed top_speed = default_speed);
        void tick() override;
        bool end() override;
    };

    class PosTrajectory : public Action {
        using traj_t = std::function<std::pair<Vec2p,bool>(duration)>;
        traj_t traj;
        time_point beg;
        angspeed last_speed;
        time_point last_call;
        PID pid_speed;
        PID pid_angle;
        bool rot_only =false;

    public:
        PosTrajectory(PosController& pc, traj_t traj, bool rot_only = false);
        void tick() override;
        bool end() override;
    };

    class PosTrajectoryA : public Action {
        using traj_t = std::function<std::pair<Pos2,bool>(duration)>;
        traj_t traj;
        time_point beg;
        angspeed last_speed;
        time_point last_call;
        PID pid_speed;
        PID pid_angle;
        bool rot_only =false;

    public:
        PosTrajectoryA(PosController& pc, traj_t traj, bool rot_only = false);
        void tick() override;
        bool end() override;
    };


    // template <typename F>
    // class SpdTrajectory : Action {
    //     F traj;
    //     PID
    //     public:
    //     Forward(PosController &pc, dist distance,
    //             speed top_speed = default_speed);
    //     void tick() override;
    //     bool end() override;
    // };



private:
    DCEncoder& left;
    DCEncoder& right;

    angle leftlast, rightlast;

    Pos2 pos{};
    speed spd;
    angspeed aspd;
    acceleration acc;

    std::unique_ptr<Action> act;
    time_point last_update;

public:
    PosController(Motors& m, acceleration nacc)
        : left(m.motors[0]), right(m.motors[1]), acc(nacc) {
        leftlast = left.get_angle();
        rightlast = right.get_angle();
    }
    void reset(Pos2 nPos) { pos = nPos; }
    void resetX(dist X) { pos.p.x = X; }
    void resetY(dist Y) { pos.p.y = Y; }
    void resetTheta(angle theta) { pos.t = theta; }


    // example of use : pc.doNow<DumbForward>(1_m);
    template <typename T, typename... Args> void doNow(Args&&... args) {
        act = std::unique_ptr<Action>(new T(*this, std::forward<Args>(args)...));
        std::cout << "present " << bool(act) << std::endl;
    }


    // TODO more complex trajectories ?

    Pos2 getPos() const { return pos; }
    speed getSpeed() const { return spd; }

    void update();
};




#ifdef BIG_ROBOT

//  ____            _        _    ____            _             _ _
// | __ )  __ _ ___| | _____| |_ / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __
// |  _ \ / _` / __| |/ / _ \ __| |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__|
// | |_) | (_| \__ \   <  __/ |_| |__| (_) | | | | |_| | | (_) | | |  __/ |
// |____/ \__,_|___/_|\_\___|\__|\____\___/|_| |_|\__|_|  \___/|_|_|\___|_|

class BasketController {
    Rotor& rot;


public:
    BasketController(Rotor& r) : rot(r) {}
    void openF(); // opens, wait 1, ejects
    void openB();
    void open();
    void rearm();
};






//     _                    ____            _             _ _
//    / \   _ __ _ __ ___  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __
//   / _ \ | '__| '_ ` _ \| |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__|
//  / ___ \| |  | | | | | | |__| (_) | | | | |_| | | (_) | | |  __/ |
// /_/   \_\_|  |_| |_| |_|\____\___/|_| |_|\__|_|  \___/|_|_|\___|_|

class ArmController {
public:
    class Action {
    protected:
        ArmController& ac;

    public:
        Action(ArmController& ac) : ac(ac) {}
        virtual ~Action() {}
        /// update the control based on local information
        virtual void tick() = 0;
        /// Tell if the action is finished
        virtual bool end() = 0;
    };


private:
    Tower& tow;
    DCEncoder& rotor;

    std::unique_ptr<Action> act;

public:
    ArmController(Rotor& r, Tower& t) : tow(t), rotor(r.rotor) {}

    void grip(int id);
    void open(int id);

    // example of use : pc.doNow<DumbForward>(1_m);
    template <typename T, typename... Args> void doNow(Args&&... args) {
        act = std::make_unique<T>(*this, std::forward<Args>(args)...);
    }


    // origin is center of robot, on the ground aligned with wheels
    // y+ is left, y- is right
    // x+ is front, x- is back
    // z+ is up, z- is down
    Vec3p pos(int id);
    Vec3s spd(int id);
    void set_goal(int id, Vec3p pos);
};


#endif
