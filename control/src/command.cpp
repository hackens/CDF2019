
#include "command.h"

#include <iostream>
#include <sstream>
#include <unistd.h>

#include "serial.h"
#include "test.h"
#include "sensors.h"
#include "arduino.h"
#include "units.h"
#include "calibration.h"
#include "geom.h"
#include "prettyprint.hpp"

using namespace std;

static bool oneTime = true;
static bool verbose = true;

namespace Colors {
  const char * Reset       = "\033[0m";
  const char * Orange      = "\033[33m";
  const char * Green       = "\033[32m";
  const char * Red         = "\033[31m";
  const char * Blue        = "\033[34m";
  const char * Purple      = "\033[35m";
  const char * Grey        = "\033[37m";
  const char * Cyan        = "\033[36m";
  const char * DarkGrey    = "\033[90m";
  const char * LightRed    = "\033[91m";
  const char * LightGreen  = "\033[92m";
  const char * Yellow      = "\033[93m";
  const char * LightBlue   = "\033[94m";
  const char * LightPurple = "\033[95m";
  const char * Turquoise   = "\033[96m";
  const char * White       = "\033[97m";
}

static const char * CMD = "[CMD]";

#define vout if(verbose) cout << Colors::Green << CMD << Colors::Reset
#define eout cout << Colors::Red << "Error:" << Colors::Reset 
#ifdef BIG_ROBOT 
static const char * NAME = "big robot";
#else
static const char * NAME = "small bot";
#endif




void cmd(int argc, char** argv, Control& ctrl){
  vector<string> vs;
  for(int i = 1 ; i < argc ; ++i){
    vs.push_back(argv[i]);
  }
  cmd(vs, ctrl);
}

static int readI(string s){
  stringstream ss(s);
  int d;
  ss >> d;
  return d;
}
static double readD(string s){
  stringstream ss(s);
  double d;
  ss >> d;
  return d;
}

void cmd(std::vector<std::string> vs, Control& ctrl){
  string cmd = vs[0];
  if(cmd == "help"){
    vout << " Help for " << NAME << " control:" << endl;
    cout << endl << "  Position and motion commands:" << endl;
    /*cout << Colors::Orange << "    f      " << Colors::Yellow << "+ <float>" << Colors::Reset << "  move foward, in cm" << endl;
    cout << Colors::Orange << "    b      " << Colors::Yellow << "+ <float>" << Colors::Reset << "  move back, in cm" << endl;
    cout << Colors::Orange << "    t      " << Colors::Yellow << "+ <float>" << Colors::Reset << "  turn base by angle (direct orientation), in degrees" << endl;*/
    cout << Colors::Orange << "    where  " << Colors::Reset << "printing current position" << endl;
    //cout << Colors::Orange << "    reset  " << Colors::Reset << "resetting position" << endl;
    cout << Colors::Orange << "    pid     " << Colors::Yellow << "[+ sp|sd|si|ap|ad|ai + <float>]" << Colors::Reset << "  get/set PID constants, unitless" << endl;
    #ifdef BIG_ROBOT
      cout << endl << "  Actuator commands:" << endl;
      cout << Colors::Orange << "    arm    "<<Colors::Yellow<<"[+ <float>]"<<Colors::Reset<<"              get/set arm angle, degrees, relative to horizontal (0°)" << endl;
      cout << Colors::Orange << "    bskt   "<<Colors::Yellow<<"+ b|f + lock|open|eject"<<Colors::Reset<<"  locks both, opens exterior or ejects interior" << endl;
      cout << Colors::Orange << "    wrst   "<<Colors::Yellow<<"+ b|f + <float>"<<Colors::Reset<<"          changes wrist orientation (relative to arm)" << endl;
      cout << Colors::Orange << "    claw   "<<Colors::Yellow<<"+ b|f + open|close"<<Colors::Reset<<"       opens or releases claw" << endl;
      //cout << Colors::Orange << "    rt     "<<Colors::Yellow<<"+ <float>"<<Colors::Reset<<"                rotate tower by angle, in degrees, relative to front" << endl;
    #endif
    cout << endl << "  Sensor commands:" << endl;
    cout << Colors::Orange << "    u      " << Colors::Reset << "print ultrasound sensors mesurement" << endl;
    cout << Colors::Orange << "    bttn   " << Colors::Reset << "print all button status" << endl;
    cout << endl << "  Other commands:" << endl;
    cout << Colors::Orange << "    check  " << Colors::Reset << "self-check" << endl;
    cout << Colors::Orange << "    help   " << Colors::Reset << "display this help" << endl;
    cout << Colors::Orange << "    int    " << Colors::Reset << "enter interactive command line" << endl;
    cout << Colors::Orange << "    exit   " << Colors::Reset << "exit interactive command line" << endl;
    cout << endl;
  }
  else if(cmd == "f"){ // avance de d cm
    if(vs.size() <= 1){
      eout << " foward command without argument" << endl;
      return;
    }
    double d = readD(vs[1]);
    vout << " Moving of " << d << " cm forward" << endl;
    // TODO : avance de d cm
  }
  else if(cmd == "b"){ // recule de d cm
    if(vs.size() <= 1){
      eout << " backward command without argument" << endl;
      return;
    }
    double d = readD(vs[1]);
    vout << " Moving of " << d << " cm backwards" << endl;
    // TODO : recule de d cm
  }
  else if (cmd == "t"){ // tourne de n degres
    if(vs.size() <= 1){
      eout << " turn command without argument" << endl;
      return;
    }
    double d = readD(vs[1]);
    vout << " Turning of " << d << "°" << endl;
    // TODO : turn n degres
  }
  else if(cmd == "where"){ // Where do I think I am ?
    vout << " Robot position : ";
    Pos2 abs = ctrl.pc.getPos();
    cout << "  x : " << abs.p.x << endl;
    cout << "  y : " << abs.p.y << endl;
    cout << "  hd: " << abs.t << endl;
  }
  else if(cmd == "reset"){ // Reset pos to start 
    vout << " Position reset" << endl;
    // TODO : reset position
  }
  else if(cmd == "pid") { // tweak pid constants
  // SPEED_KP ANGLE_KP
  // SPEED_KI ANGLE_KI
  // SPEED_KD ANGLE_KD
    if(vs.size() <= 1){
      vout << " PID constant values" << endl << endl;
      /*cout << "  SPEED_KP = " << SPEED_KP << endl;
      cout << "  SPEED_KI = " << SPEED_KI << endl;
      cout << "  SPEED_KD = " << SPEED_KD << endl << endl;
      cout << "  ANGLE_KP = " << ANGLE_KP << endl;
      cout << "  ANGLE_KI = " << ANGLE_KI << endl;
      cout << "  ANGLE_KD = " << ANGLE_KD << endl;*/
      return;
    }
    else if (vs.size() <= 3) {
      eout << " pid doesn't have enough arguments" << endl;
      cout << "     usage: pid [+ sp|sd|si|ap|ad|ai + <float>]" << endl;
    }
    double val = readD(vs[2]);
    string k = vs[1];
    if (k == "sp") {
      vout << " Setting SPEED_KP to " << val << endl;
      //SPEED_KP = val;
    }
    else if (k == "si") {
      vout << " Setting SPEED_KI to " << val << endl;
      //SPEED_KI = val;
    }
    else if (k == "sd") {
      vout << " Setting SPEED_KD to " << val << endl;
      //SPEED_KD = val;
    }
    else if (k == "ap") {
      vout << " Setting ANGLE_KP to " << val << endl;
      //ANGLE_KP = val;
    }
    else if (k == "sp") {
      vout << " Setting ANGLE_KI to " << val << endl;
      //ANGLE_KI = val;
    }
    else if (k == "sp") {
      vout << " Setting ANGLE_KD to " << val << endl;
      //ANGLE_KD = val;
    }
    else {
      eout << " unknown constant name \"" << k << "\"" << endl;
      cout << "   use one of sp, si, sd, ap, ai, ad" << endl;
    }
  }

  #ifdef BIG_ROBOT
    else if(cmd == "arm"){ // get/set arm angle
      if(vs.size() <= 1){
          vout << " arm angle " << (ctrl.tow.arm.pos() - 150_deg).degrees() << " degrees" << endl;
        return;
      }
      double a = readD(vs[1]);
      if (a > -35 || a < 35) {
        vout << " setting arm angle to " << a << " deg" << endl;
        ctrl.tow.arm.set_pos(((angle) (a*M_PI/180)) + 150_deg);
        return;
      }
      eout << " arm: invalid angle " << a << " deg, must be in [-35,35]" << endl;
      return;
    }
    else if(cmd == "bskt") { // locks, opens or closes basket
      if (vs.size() <= 2) {
        eout << " bskt command doesn't have enough arguments\n        usage: bskt b|f lock|open|eject" << endl;
        return;
      }
      string i = vs[1];
      int nb = -1;
      if (i == "f")
        nb = Rotor::Front;
      else if (i == "b")
        nb = Rotor::Back;
      else {
        eout << " bskt: invalid argument, first arg must be \"b\" or \"f\"" << endl;
        return;
      }
      string arg = vs[2];
      if (arg == "open") {
        vout << " bskt: opening basket " << i << endl;
        ctrl.rot.baskets[nb][Rotor::Down].set_pos(basket::open);
      }
      else if (arg == "lock") {
        vout << " bskt: locking basket " << i << endl;
        ctrl.rot.baskets[nb][Rotor::Down].set_pos(basket::close);
        ctrl.rot.baskets[nb][Rotor::Up].set_pos(basket::retract);
      }
      else if (arg == "eject") {
        ctrl.rot.baskets[nb][Rotor::Up].set_pos(basket::eject);
        vout << " bskt: ejecting basket " << i << endl;
      }
    }
    else if(cmd == "wrst") { // wrist
      if(vs.size() <= 2){
        eout << " wrst command doesn't have enough arguments\n      usage: wrst f|b angle" << endl;
        return;
      }
      string i = vs[1];
      int nb = -1;
      if (i == "f")
        nb = 0;
      else if (i == "b")
        nb = 1;
      else {
        eout << " wrst: invalid argument, first arg must be \"b\" or \"f\"" << endl;
        return;
      }
      double d = readD(vs[2]);
      if (d >= 0 && d <= 180) {
        vout << " setting wrist " << 2*nb <<" to " << d << " degres" << endl;
        ctrl.tow.wrist[nb].set_pos((angle) (d*M_PI/180));
        return;
      }
      vout << " wrst: invalid angle " << d << " deg (must be in [0,180]" << endl;
      return;
    }
    else if(cmd == "claw") { // claw
      if(vs.size() <= 1){
        eout << " claw command doesn't have enough arguments\n      usage: claw f|b open|close" << endl;
        return;
      }
      string i = vs[1];
      int nb = -1;
      if (i == "f")
        nb = 0;
      else if (i == "b")
        nb = 1;
      else {
        eout << " claw: invalid argument, first arg must be \"b\" or \"f\"" << endl;
        return;
      }
      string arg = vs[2];
      if (arg == "open") {
        vout << " opening claw 0" << endl;
        ctrl.tow.grippers[nb].set_pos(claw_open);
      }
      else if (arg == "close") {
        vout << " closing claw 0" << endl;
        ctrl.tow.grippers[nb].set_pos(claw_closed);
      }
    }
  #endif

  else if (cmd == "u") { // capteurs ultrason
      vout << " ultrasound captors read:" << endl;
      #ifdef BIG_ROBOT
        cout << "  Captor 0 : " << ctrl.rot.ds[0].get() << endl;
        cout << "  Captor 1 : " << ctrl.rot.ds[0].get() << endl;
      #else
        cout << "  Captor : " << ctrl.mot.ds.get() << endl;
      #endif

  }
  else if (cmd == "bttn") { // boutons
    vout << " button states:" << endl;
    auto f = [](const char * name, Button b) { 
      if (b.pressed())
        cout << "    " << name << Colors::Green << "on" << Colors::Reset << endl;
      else
        cout << "    " << name << Colors::Red << "off" << Colors::Reset << endl; 
    };
    f("starter:   ", ctrl.mot.starter);
    #ifdef BIG_ROBOT
      f("contact 0: ", ctrl.mot.contact[0]);
      f("contact 1: ", ctrl.mot.contact[1]);
      f("claw 0:    ", ctrl.tow.gripped[0]);
      f("claw 1:    ", ctrl.tow.gripped[1]);
    #else
      f("bau:       ", ctrl.mot.bau);
    #endif
  }


  else if(cmd == "check"){ // self-check
    vout << " self-check" << endl;
    selfcheck(ctrl.serials);
  }
  else if(cmd == "int"){ // enters interactive mode
    inter(ctrl);
  }
  else if(cmd == "exit"){ // do you really need a comment here ?
    exit(0);
  }
  else{
    eout << "Unknown command " << cmd << ", type \"help\" for list of commands." << endl;
  }
}

void inter(Control &ctrl){
  while(true){
    cout << Colors::Cyan << NAME << Colors::Reset << "$ ";
    char buffer[100];
    cin.getline(buffer,100);
    if(cin){
      string s = buffer;
      stringstream ss(s);
      vector<string> vs;
      while(ss){
        string s;
        ss >> s;
        vs.push_back(s);
      }
      cmd(vs, ctrl);
    }
  }
}
