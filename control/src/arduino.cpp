#include "arduino.h"
#include "calibration.h"

using namespace std;


// All mapping from Arduino IDs to actual item place must be done here.

Motors::Motors(Serial &s)
    : motors{DCEncoder(s, 0, false, 1), DCEncoder(s, 1, true, 1)},
      starter(s, 0),
#ifdef BIG_ROBOT
      contact {Button(s, 1), Button(s, 2)}
#else
      bau(s, 1), ds(s, 0)
#endif
{}

void Motors::update() {
    for(auto& m : motors) m.update();
}

#ifdef BIG_ROBOT

Rotor::Rotor(Serial& s)
    : rotor(s, 0, false, rotor_reduction),
      baskets{{BasicServo(s, 0, 90_deg, 1s),
               BasicServo(s, 1, 90_deg, 1s)},
              {BasicServo(s, 2, 90_deg, 1s),
               BasicServo(s, 3, 90_deg, 1s)}},
      ds{DistSensor(s, 0), DistSensor(s, 1)}
{}

void Rotor::update() {
    rotor.update();
}


Tower::Tower(Serial& s)
    : arm(s,0),
      wrist{BasicServo(s, 0, 90_deg, 1s), BasicServo(s, 1, 90_deg, 1s)},
      grippers{BasicServo(s, 2, 90_deg, 1s), BasicServo(s, 3, 90_deg, 1s)},
      gripped{Button(s,0),Button(s,1)}
{}

void Tower::update() {
    arm.update();
}



#endif
