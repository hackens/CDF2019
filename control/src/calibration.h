#pragma once

// Every constant that is supposed to be tweaked according to the actual
// physical properties of the robot should be defined here
// Board distance and other non-robot characteristics are in board.h

#include "units.h"

//length unit : mm
// angle unit : rad
//  time unit : s

// independent constant
constexpr dist wheel_diameter = 28.7_cm/M_PI;
constexpr dist wheel_radius = wheel_diameter/2;
constexpr angle angle_per_tick = 1_turn / 1400;
constexpr speed default_speed = 5_cmps;
constexpr speed speed_error = 0.1_mmps;
constexpr angspeed angspeed_error = 30_degps;
constexpr angle angle_error = 1_turn/70;
constexpr double temperature = 25;
constexpr speed sound_speed = speed::meters_per_second(20.05*sqrt(273.15 + temperature));

//TODO
constexpr angle claw_closed = 0_deg; // or 180
constexpr angle claw_open = 90_deg;


#ifdef BIG_ROBOT
// big robot constants


//motors
constexpr dist wheel_distance = 33.5_cm;
constexpr dist dist_center_to_wall = 17_cm; // approx arm length/2
constexpr speed max_speed = 30_cmps;
// constexpr speed gripping_speed = 10_cm / 20s; // TODO in function of other parameters

//rotor
constexpr double rotor_reduction = 10./46.;

//towers
constexpr dist arm_length = 34_cm;
constexpr angle catching_angle= -20_deg;
constexpr angle baskets_angle = 20_deg; // I assume this is symmetric
constexpr dist shoulder_height = 270_mm;
constexpr dist gripper_length = 75_mm;

// grippers
constexpr angle opening_angle = 10_deg;

//baskets
namespace basket{
    constexpr angle origin[2][2] = {{0_deg,0_deg},{0_deg,0_deg}};
    //exterior
    constexpr angle close = 0_deg;
    constexpr angle open = 90_deg; // or 180
    // interior
    constexpr angle eject = 90_deg; // or 180
    constexpr angle retract = 0_deg;
}


#else
constexpr dist wheel_distance = 20_cm;
// small robot constants
#endif
