#ifndef COMMAND_H
#define COMMAND_H

#include <vector>
#include <string>

#include "serial.h"
#include "test.h"
#include "arduino.h"
#include "controllers.h"

struct Control{
  Serial** serials; // used for self-check
  Motors mot;
  PosController pc; // in BIG_ROBOT ??
  #ifdef BIG_ROBOT
    Rotor rot;
    Tower tow;
  #endif
};

void cmd(int argc, char** argv, Control&);

void cmd(std::vector<std::string> vs, Control&);

void inter(Control&);

#endif
