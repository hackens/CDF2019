#pragma once
#include <string>
#include <cstring>

class Serial {
    public:
        Serial(const std::string& filename);
        ~Serial();
        void waitUntilReady();
        void write(const void* buf, size_t nbyte);
        void read(void* buf, size_t nbyte);
        char getId();

        template<typename T>
        void put(const T& t){
            write(reinterpret_cast<const char*>(&t), sizeof(T));
        }

        template<typename T, typename ... Args>
        void put(const T& t, const Args ... args){
            put(t);
            put(args...);
        }

        void put(const char* t){
            write(t, strlen(t));
        }

        template<typename T>
        T get(){
            T t;
            read(reinterpret_cast<char*>(&t), sizeof(T));
            return t;
        }

    private:
        int _fd = -1;
};

