#pragma once

#include "units.h"

// geometry : vector, matrix, ... goes here

template <class T> struct Vec2 {
    T x, y;
    Vec2& operator+=(const Vec2& o) {
        x += o.x;
        y += o.y;
        return *this;
    }
    Vec2& operator-=(const Vec2& o) {
        x -= o.x;
        y -= o.y;
        return *this;
    }
    Vec2& operator*=(double f) {
        x *= f;
        y *= f;
        return *this;
    }
    Vec2& operator/=(double f) {
        x /= f;
        y /= f;
        return *this;
    }
    friend Vec2 operator+(Vec2 v, const Vec2& v2) { return v += v2; }
    friend Vec2 operator-(Vec2 v, const Vec2& v2) { return v -= v2; }
    friend Vec2 operator*(Vec2 v, double f) { return v *= f; }
    friend Vec2 operator*(double f, Vec2 v) { return v *= f; }
    friend Vec2 operator/(Vec2 v, float f) { return v /= f; }

    T norm() const {
        return add_unit<T>(sqrt(remove_unit(x) * remove_unit(x) +
                                remove_unit(y) * remove_unit(y)));
    }

    friend std::ostream& operator<<(std::ostream& out, Vec2 v) {
        return out << "(" << v.x << ", " << v.y << ")";
    }

    static Vec2 polar(T r, angle a) { return Vec2{r * cos(a), r * sin(a)}; }
    std::pair<T, angle> get_polar() const {
        return std::pair(norm(), atan2(remove_unit(y), remove_unit(x)));
    }

    Vec2 rotate_around(Vec2 v, angle theta) const {
        Vec2 diff = *this - v;
        auto [n, t] = get_polar();
        return polar(n, t + theta) + v;
    }

    T project(Vec2<double> v) {
        v /= v.norm();
        T t = x * v.x + y * v.y;
        return t;
    }
};

template <typename T, typename U> auto operator*(Vec2<T> v, U u) {
    Vec2<decltype(v.x * u)> res;
    res.x = v.x * u;
    res.y = v.y * u;
    return res;
}

template <typename T, typename U> auto operator*(U u, Vec2<T> v) {
    Vec2<decltype(u * v.x)> res;
    res.x = u * v.x;
    res.y = u * v.y;
    return res;
}


template <typename T, typename U> auto operator/(Vec2<T> v, U u) {
    Vec2<decltype(v.x / u)> res;
    res.x = v.x / u;
    res.y = v.y / u;
    return res;
}


using Vec2d = Vec2<double>;
using Vec2p = Vec2<dist>;
using Vec2s = Vec2<speed>;
using Vec2a = Vec2<acceleration>;

template <class T>
Vec2d remove_unit(Vec2<T> vec) {
    return {remove_unit(vec.x), remove_unit(vec.y)};
}

template <typename T> Vec2d normalize(Vec2<T> v) { return v / v.norm(); }

template <typename T> double angle_between(Vec2<T> u, Vec2<T> v) {
    Vec2d a = remove_unit(u);
    Vec2d b = remove_unit(v);
    double dot = a.x*b.x + a.y*b.y;
    double cross = a.x*b.y - a.y*b.x;
    return atan2(cross, dot);
}


//  ____           ____
// |  _ \ ___  ___|___ \
// | |_) / _ \/ __| __) |
// |  __/ (_) \__ \/ __/
// |_|   \___/|___/_____|

struct Pos2 {
    Vec2p p;
    angle t;
};





// __     __        _____
// \ \   / /__  ___|___ /
//  \ \ / / _ \/ __| |_ \
//   \ V /  __/ (__ ___) |
//    \_/ \___|\___|____/

template <class T> struct Vec3 {
    T x, y, z;
    Vec3& operator+=(const Vec3& o) {
        x += o.x;
        y += o.y;
        z += o.z;
        return *this;
    }
    Vec3& operator+=(T o) {
        x += o;
        y += o;
        z += o;
        return *this;
    }
    Vec3& operator-=(const Vec3& o) {
        x -= o.x;
        y -= o.y;
        z -= o.z;
        return *this;
    }
    Vec3& operator*=(double f) {
        x *= f;
        y *= f;
        z *= f;
        return *this;
    }
    Vec3& operator/=(double f) {
        x /= f;
        y /= f;
        z /= f;
        return *this;
    }
    Vec3 operator-() const { return Vec3{-x, -y, -z}; }
    Vec3 operator-(T r) const { return Vec3{x - r, y - r, z - r}; }
    friend Vec3 operator+(Vec3 v, const Vec3& v2) { return v += v2; }
    friend Vec3 operator+(Vec3 v, T f) { return v += f; }
    friend Vec3 operator-(Vec3 v, const Vec3& v2) { return v -= v2; }
    friend Vec3 operator*(Vec3 v, double f) { return v *= f; }
    friend Vec3 operator*(double f, Vec3 v) { return v *= f; }
    friend Vec3 operator/(Vec3 v, T f) { return v /= f; }

    T norm() const {
        return add_unit<T>(sqrt(remove_unit(x) * remove_unit(x) +
                                remove_unit(y) * remove_unit(y) +
                                remove_unit(z) * remove_unit(z)));
    }

    friend std::ostream& operator<<(std::ostream& out, Vec3 v) {
        return out << "(" << v.x << ", " << v.y << ", " << v.z << ")";
    }

    static Vec3 spherical(T r, angle phi, angle theta) {
        return Vec3{r * cos(phi) * sin(theta), r * sin(phi) * sin(theta),
                    r * cos(theta)};
    }
    // phi then theta
    std::tuple<T, angle, angle> get_spherical() const {
        return std::tuple(
            norm(), atan2(remove_unit(y), remove_unit(x)),
            atan2(remove_unit(Vec2<T>{x, y}.norm()),remove_unit(z)));
    }
};

using Vec3d = Vec3<double>;
using Vec3p = Vec3<dist>;
using Vec3s = Vec3<speed>;
using Vec3a = Vec3<acceleration>;

static Vec3d X{1, 0, 0};
static Vec3d Y{0, 1, 0};
static Vec3d Z{0, 0, 1};

template <typename T, typename U> auto operator*(Vec3<T> v, U u) {
    Vec3<decltype(v.x * u)> res;
    res.x = v.x * u;
    res.y = v.y * u;
    res.z = v.z * u;
    return res;
}

template <typename T, typename U> auto operator*(U u, Vec3<T> v) {
    Vec3<decltype(u * v.x)> res;
    res.x = u * v.x;
    res.y = u * v.y;
    res.z = u * v.z;
    return res;
}


template <typename T, typename U> auto operator/(Vec3<T> v, U u) {
    Vec3<decltype(v.x / u)> res;
    res.x = v.x / u;
    res.y = v.y / u;
    res.z = v.z / u;
    return res;
}
