#pragma once

#include "units.h"

/// Class that do the serial communications
class Serial;

//Low level component
class LLComp{
protected:
    Serial& serial;
public:
    LLComp(Serial& nserial, u8 nid) : serial(nserial), id(nid){}
    const u8 id;
};


class LLBasicServo : public LLComp{
public:
    using LLComp::LLComp;
    void set_pos(angle ang);
};

class LLNiceServo : public LLComp{
public:
    using LLComp::LLComp;
    void set_pos(angle ang);
    void set_spd(angspeed ang);
    angle pos();
    angspeed spd();
};

class LLDCEncoder : public LLComp{
public:
    LLDCEncoder(Serial& nserial, u8 nid, bool nrev) : LLComp(nserial, nid), rev(nrev) {}
    i32 get_ticks();
    void set_voltage(u8 val, bool dir);
    constexpr std::pair<u8, bool> last_voltage() const { return _last_voltage; }
protected:
    std::pair<u8, bool> _last_voltage;
public:
    bool rev;
};

class LLDistSensor : public LLComp{
public:
    using LLComp::LLComp;
    duration poll_time();
};

class LLButton : public LLComp{
public:
    using LLComp::LLComp;
    bool pressed();
};
