#include "controllers.h"
#include "units.h"
#include <thread>

using namespace std;
using namespace std::literals;

PosController::DumbForward::DumbForward(PosController& pc, dist distance,
                                        bool dir, speed top_speed)
    : Action(pc), distance(distance), dir(dir ? 1 : -1), top_speed(top_speed),
      pid(0.1, 0.1, 0.1) {

    duration ramp_dur = top_speed / pc.acc;
    ramp_len = 0.5 * pc.acc * ramp_dur * ramp_dur;
    if (2 * ramp_len > distance) ramp_len = distance / 2;
    last_pos = pc.getPos().p;
}

void PosController::DumbForward::tick() {
    Vec2p mov = pc.getPos().p - last_pos;
    dist len = mov.norm();
    done += len;
    cout << "phase" << phase <<endl;
    // speed target;
    if (done < ramp_len) {
        // accelerating phase
        speed spd = pc.acc * (now() - start_phase);
        if (spd > top_speed) spd = top_speed;
        pc.left.set_spd(dir * spd / wheel_diameter);
        pc.right.set_spd(dir * spd / wheel_diameter);
    }
    else if (distance - done < ramp_len) {
        if (phase != Decelerating) {
            start_phase = now();
            phase = Decelerating;
        }
        // decelerating phase

        speed lin_speed = top_speed - pc.acc * (now() - start_phase);
        if (lin_speed < 0_mps) lin_speed = 0_mps;
        double coeff = lin_speed / top_speed;
        speed pid_speed = top_speed * pid.update(remove_unit(distance - done));

        speed spd = lin_speed * coeff + pid_speed * (1 - coeff);
        pc.left.set_spd(dir * spd / wheel_diameter);
        pc.right.set_spd(dir * spd / wheel_diameter);
    }
    else {
        if (phase != Stable) {
            start_phase = now();
            phase = Stable;
        }
        pc.left.set_spd(dir * top_speed / wheel_diameter);
        pc.right.set_spd(dir * top_speed / wheel_diameter);
    }
}

bool PosController::DumbForward::end() {
    return (pc.getSpeed().abs() < speed_error) and
           ((distance - done) / wheel_diameter).abs() < angle_error;
}


const double SPEED_KP = 0.5;
const double SPEED_KI = 0;
const double SPEED_KD = 0;
const double ANGLE_KP = 2;
const double ANGLE_KI = 0;
const double ANGLE_KD = 1;

PosController::PosTrajectory::PosTrajectory(PosController& pc, traj_t traj,
                                            bool rot_only)
    : Action(pc), traj(traj), beg(now()), last_speed(0_radps), last_call(now()),
      pid_speed(SPEED_KP, SPEED_KI, SPEED_KD),
      pid_angle(ANGLE_KP, ANGLE_KI, ANGLE_KD), rot_only(rot_only) {}

void PosController::PosTrajectory::tick() {
    time_point tp = now();
    auto [goal,_] = traj(tp - beg);
    Pos2 cur = pc.getPos();
    duration dt = tp - last_call;

    dist error_distp = (goal - cur.p).project(Vec2d::polar(1, cur.t));
    dist error_dist = (goal - cur.p).norm();
    cout << "error_dist  " << error_dist << " error_distp " << error_distp ;
    double error_angle = angle_between(goal - cur.p, Vec2p::polar(1_m, cur.t));
    // cout << "error_angle " << error_angle << endl;

    if (abs(error_angle) > M_PI / 2) {
        // error_dist = - error_dist;
        if (error_angle > 0) error_angle -= M_PI;
        else if (error_angle < 0) error_angle += M_PI;
    }

    cout << " error_angle " << error_angle << endl;

    angspeed act_speed = angspeed::rad_per_second(
        pid_speed.update(remove_unit(error_distp)) / remove_unit(wheel_radius));
    cout << "act_speed  " << act_speed;
    angspeed act_angle =
        angspeed::rad_per_second(pid_angle.update(error_angle));
    if (remove_unit(error_dist) < 1) act_angle *= remove_unit(error_dist);
    cout << " act_angle  " << act_angle;
    // angspeed speed_max_diff_abs =
    // angspeed::rad_per_second(remove_unit((2*pc.acc)*dt)/remove_unit(wheel_radius));
    // cout << "speed_max_diff_abs  " << speed_max_diff_abs << endl;

    // act_speed = std::min(std::max(
    //     act_speed,
    //     last_speed - speed_max_diff_abs),
    //     last_speed + speed_max_diff_abs
    // );
    if (rot_only) act_speed = 0_rpm;

    if(act_speed > 300_degps){
        act_speed = 300_degps;
    }
    else if(act_speed < -300_degps){
        act_speed = -300_degps;
    }


    cout << " act_speed2  " << act_speed << endl;

    cout << "left_speed " << act_speed + act_angle;
    cout << " right_speed " << act_speed - act_angle << endl;




    pc.left.set_spd(act_speed + act_angle);
    pc.right.set_spd(act_speed - act_angle);

    last_call = tp;
    last_speed = act_speed;
}

bool PosController::PosTrajectory::end() {
    auto [goal,end] = traj(now()-beg);
    return end and (goal - pc.getPos().p).norm() < 1_cm;
}





const double SPEED_KPA = 0.5;
const double SPEED_KIA = 0;
const double SPEED_KDA = 0;
const double ANGLE_KPA = 2;
const double ANGLE_KIA = 0;
const double ANGLE_KDA = 1;

PosController::PosTrajectoryA::PosTrajectoryA(PosController& pc, traj_t traj,
                                            bool rot_only)
    : Action(pc), traj(traj), beg(now()), last_speed(0_radps), last_call(now()),
      pid_speed(SPEED_KP, SPEED_KI, SPEED_KD),
      pid_angle(ANGLE_KP, ANGLE_KI, ANGLE_KD), rot_only(rot_only) {}

void PosController::PosTrajectoryA::tick() {
    // time_point tp = now();
    // auto [goal,_] = traj(tp - beg);
    // Pos2 cur = pc.getPos();
    // duration dt = tp - last_call;

    // dist error_distp = (goal - cur.p).project(Vec2d::polar(1, cur.t));
    // dist error_dist = (goal - cur.p).norm();
    // cout << "error_dist  " << error_dist << " error_distp " << error_distp ;
    // double error_angle = angle_between(goal - cur.p, Vec2p::polar(1_m, cur.t));
    // // cout << "error_angle " << error_angle << endl;

    // if (abs(error_angle) > M_PI / 2) {
    //     // error_dist = - error_dist;
    //     if (error_angle > 0) error_angle -= M_PI;
    //     else if (error_angle < 0) error_angle += M_PI;
    // }

    // cout << " error_angle " << error_angle << endl;

    // angspeed act_speed = angspeed::rad_per_second(
    //     pid_speed.update(remove_unit(error_distp)) / remove_unit(wheel_radius));
    // cout << "act_speed  " << act_speed;
    // angspeed act_angle =
    //     angspeed::rad_per_second(pid_angle.update(error_angle));
    // if (remove_unit(error_dist) < 1) act_angle *= remove_unit(error_dist);
    // cout << " act_angle  " << act_angle;
    // // angspeed speed_max_diff_abs =
    // // angspeed::rad_per_second(remove_unit((2*pc.acc)*dt)/remove_unit(wheel_radius));
    // // cout << "speed_max_diff_abs  " << speed_max_diff_abs << endl;

    // // act_speed = std::min(std::max(
    // //     act_speed,
    // //     last_speed - speed_max_diff_abs),
    // //     last_speed + speed_max_diff_abs
    // // );
    // if (rot_only) act_speed = 0_rpm;

    // if(act_speed > 300_degps){
    //     act_speed = 300_degps;
    // }
    // else if(act_speed < -300_degps){
    //     act_speed = -300_degps;
    // }


    // cout << " act_speed2  " << act_speed << endl;

    // cout << "left_speed " << act_speed + act_angle;
    // cout << " right_speed " << act_speed - act_angle << endl;




    // pc.left.set_spd(act_speed + act_angle);
    // pc.right.set_spd(act_speed - act_angle);

    // last_call = tp;
    // last_speed = act_speed;
}

bool PosController::PosTrajectoryA::end() {
    auto [goal,end] = traj(now()-beg);
    // return end and (goal - pc.getPos().p).norm() < 1_cm;
}




//  _   _           _       _
// | | | |_ __   __| | __ _| |_ ___
// | | | | '_ \ / _` |/ _` | __/ _ \
// | |_| | |_) | (_| | (_| | ||  __/
//  \___/| .__/ \__,_|\__,_|\__\___|
//       |_|

void PosController::update() {
    left.update();
    right.update();

    static dist len_sum = 0_m;


    speed leftspd = left.spd() * wheel_radius;
    speed rightspd = right.spd() * wheel_radius;

    spd = (leftspd + rightspd) / 2;
    aspd = (leftspd - rightspd) / wheel_distance;


    angle leftangle = left.get_angle();
    angle rightangle = right.get_angle();


    dist leftdiff = (leftangle - leftlast) * wheel_radius;
    dist rightdiff = (rightangle - rightlast) * wheel_radius;

    // cout << "l " << leftdiff << "r " << rightdiff << endl;
    // cout << "l " << left.rev << "r " << right.rev << endl;

    // cout << len_sum << endl;

    // compute instantaneous center of rotation and rotate around it
    dist len = (leftdiff + rightdiff) / 2;

    angle dtheta = -(leftdiff- rightdiff) / (wheel_distance);



    // cout << "len " << len << "dt " << dtheta << endl;

    len_sum += len;

    pos.p += Vec2p::polar(len,pos.t);
    pos.t += dtheta;

    leftlast = leftangle;
    rightlast = rightangle;



    if (act){
        act->tick();
    }
    if (act and act->end()){
        act = nullptr;
        left.set_spd(0_rpm);
        right.set_spd(0_rpm);
    }
}


#ifdef BIG_ROBOT
void BasketController::openF() {
    auto& up = rot.baskets[Rotor::Front][Rotor::Up];
    auto& down = rot.baskets[Rotor::Front][Rotor::Down];
    down.set_pos(basket::open);
    while (!down.is_done());
    std::this_thread::sleep_for(1s);
    up.set_pos(basket::eject);
    while (!up.is_done());
}

void BasketController::openB() {
    auto& up = rot.baskets[Rotor::Back][Rotor::Up];
    auto& down = rot.baskets[Rotor::Back][Rotor::Down];
    down.set_pos(basket::open);
    while (!down.is_done());
    std::this_thread::sleep_for(1s);
    up.set_pos(basket::eject);
    while (!up.is_done());
}

void BasketController::open() {
    openF();
    openB();
}

void BasketController::rearm() {
    for (auto& b1 : rot.baskets) {
        b1[Rotor::Up].set_pos(basket::retract);
        b1[Rotor::Down].set_pos(basket::close);
    }
}


Vec3p ArmController::pos(int id) {
    assert(id >= 0 and id < 2);
    // rotor at angle 0 is gripper 1 on front, gripper 2 on back

    angle phi = rotor.get_angle();

    // around y
    angle arm_angle = tow.arm.pos() * (id == 0 ? 1. : -1.);
    angle wrist_angle = tow.wrist[id].pos();

    Vec3p res = shoulder_height * Z;
    res += Vec3p::spherical(arm_length / 2, phi, arm_angle + 90_deg);
    // TODO check wrist_angle sign
    res +=
        Vec3p::spherical(gripper_length, phi, wrist_angle + arm_angle - 90_deg);

    return res;
}


Vec3s ArmController::spd(int id) {
    assert(id >= 0 and id < 2);
    Vec3p p = pos(id);
    angspeed dphi = rotor.spd();

    angspeed darm = tow.arm.spd() * (id == 0 ? 1. : -1.);

    p -= shoulder_height * Z;

    auto [len, phi, theta] = p.get_spherical();

    Vec3s res;

    res += len * dphi * Vec3d::spherical(1,phi+90_deg,90_deg);
    res += len * darm * Vec3d::spherical(1,phi,theta + 90_deg);

    // Wrist movement unaccounted for since basic servo do not have
    // speed feedback

    return res;
}

void ArmController::set_goal(int id, Vec3p ori) {
    assert(id >= 0 and id < 2);
    // y+ is left, y- is right
    // x+ is front, x- is back
    // z+ is up, z- is down

    auto [plen, theta_z] = (Vec2p {.x = ori.x, .y = ori.y}).get_polar();
    if(id == 1) theta_z += 180_deg; // rotor at angle 0 is gripper 1 on front, 2 on back
    if(theta_z > 180_deg) theta_z = theta_z - 360_deg;

    Vec3p ori_from_shoulder = ori - shoulder_height * Z;
    Vec2p ori_in_arm_plane = Vec2p {.x = plen, .y = ori_from_shoulder.z};
    double a = remove_unit(ori_in_arm_plane.norm());
    double b = remove_unit(gripper_length);
    double c = remove_unit(arm_length / 2);

    angle theta_wrist = angle(acos((b*b + c*c - a*a)/(2*b*c))) - 90_deg;
    angle theta_arm   = angle(acos((a*a + c*c - b*b)/(2*a*c)))
                          - ori_in_arm_plane.get_polar().second;
    if(id == 1) theta_arm *= -1;

    tow.arm.set_pos(theta_arm);
    tow.wrist[id].set_pos(theta_wrist);
    // TODO set rotor to theta_z

    // std::pair<dist, angle> t = (Vec2p {.x = ori.x, .y = ori.y}).get_polar();
    // angle rot_tgt = t.second;
    // // rotor at angle 0 is gripper 1 on front, gripper 2 on back
    // if(id == 1)
    //     rot_tgt += 180_deg;
    // if(rot_tgt > 180_deg)
    //     rot_tgt -= 360_deg;
    // ori -= shoulder_height * Z;
    // Vec2p tgt = Vec2p{.x = t.first, .y = ori.z};
    // double a = remove_unit(tgt.norm());
    // double b = remove_unit(gripper_length);
    // double c = remove_unit(arm_length) / 2;
    // angle wrist_tgt = acos((a*a - b*b - c*c)/(2*b*c)); // Al Kashi
    // angle arm_tgt = acos((b*b - a*a - c*c)/(2*a*c)); // Al Kashi
    // if(id==1)
    //     arm_tgt *= -1;
    // tow.arm.set_pos(arm_tgt);
    // tow.wrist[id].set_pos(wrist_tgt);
    // // TODO set rotor to rot_tgt

}


#endif
