#include "test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <poll.h>
#include <signal.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "serial.h"

using namespace std;

static sig_atomic_t end = 0;

static termios oldtio;

static void disable_non_canonical() {
    tcsetattr(0, TCSANOW, &oldtio);
}

static void sighandler(int signo) {
    disable_non_canonical();
    exit(130);
}

static void enable_non_canonical() {
    struct termios curtio;
    struct sigaction sa;

    /* Save stdin terminal attributes */
    tcgetattr(0, &oldtio);

    /* Make sure we exit cleanly */
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = sighandler;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGQUIT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);

    /* This is needed to be able to tcsetattr() after a hangup (Ctrl-C)
     * see tcsetattr() on POSIX
     */
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = SIG_IGN;
    sigaction(SIGTTOU, &sa, NULL);

    /* Set non-canonical no-echo for stdin */
    tcgetattr(0, &curtio);
    curtio.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(0, TCSANOW, &curtio);
}

static bool getKeypress(char& out) {
    struct pollfd pfds[1];
    int ret;

    /* See if there is data available */
    pfds[0].fd = 0;
    pfds[0].events = POLLIN;
    ret = poll(pfds, 1, 0);

    /* Consume data */
    if (ret > 0) {
        read(0, &out, 1);
        return true;
    }
    return false;
}

static bool skip() {
    char c;
    while(!getKeypress(c));
    return c == '\n';
}

void check(LLDCEncoder& m) {
    char c;
    printf("  Testing motor %d\n", int(m.id));
    if(!skip()) {
        printf("    Forward fast\n");
        m.set_voltage(255, true);
        while(!getKeypress(c));
        printf("    Backward fast\n");
        m.set_voltage(255, false);
        while(!getKeypress(c));
        printf("    Forward slow\n");
        m.set_voltage(50, true);
        while(!getKeypress(c));
        printf("    Backward slow\n");
        m.set_voltage(50, false);
        while(!getKeypress(c));
        printf("    Encoder\n");
        m.set_voltage(0, false);
        while(!getKeypress(c)) {
            printf("\r      %d          ", m.get_ticks());
            fflush(stdout);
        }
        printf("\n");
    } else {
        printf("  > Skipped\n");
    }
}

void check(LLDistSensor& ds) {
    char c;
    printf("  Testing distance sensor %d\n", int(ds.id));
    const double TEMP = 25;
    const speed SOUND_SPEED = speed::meters_per_second(20.05*sqrt(273.15 + TEMP));
    while(!getKeypress(c)) {
        printf("\r    %lf             ", (SOUND_SPEED*(ds.poll_time()/2)).cm());
        fflush(stdout);
    }
    printf("\n");
}

void check(LLBasicServo& bs) {
    char c;
    printf("  Testing servo %d\n", int(bs.id));
    int angles[4] = {0,90,180,90};
    int cur = 0;
    while(!getKeypress(c)) {
        printf("    %d    \r", angles[cur]);
        fflush(stdout);
        bs.set_pos(angle(angles[cur]/180.0*M_PI));
        sleep(1);
        cur = (cur+1)%4;
    }
    printf("\n");
}

void check(LLButton& b) {
    char c;
    printf("  Testing button %d\n", int(b.id));
    while(!getKeypress(c)) {
        if(b.pressed()) {
            printf("\r    On ");
        } else {
            printf("\r    Off");
        }
        fflush(stdout);
    }
    printf("\n");
}

void check(LLNiceServo& b) {
    char c;
    printf("  Testing dynamixel servo %d\n", int(b.id));
    angle angles[4] = {140_deg,150_deg,160_deg,150_deg};
    int cur = 0;

    printf("    Moving fast\n");
    b.set_spd(10_rpm);
    while(!getKeypress(c)) {
        cout << "      " << angles[cur] << "\r";
        fflush(stdout);
        b.set_pos(angles[cur]);
        sleep(1);
        cur = (cur+1)%4;
    }
    printf("    Moving slow\n");
    b.set_spd(2_rpm);
    while(!getKeypress(c)) {
        cout << "      " << angles[cur] << "\r";
        fflush(stdout);
        b.set_pos(angles[cur]);
        sleep(2);
        cur = (cur+1)%4;
    }
    printf("    Angle Print\n");
    while(!getKeypress(c)) {
        cout << "      " << b.pos() << "\r";
        fflush(stdout);
    }
    printf("\n");
}


void selfcheck(Serial** serials) {
    enable_non_canonical();

#ifdef BIG_ROBOT
    for(int i = 0; i < 3; ++i) {
        if(serials[i] != nullptr) {
            printf("Arduino %d: connected\n", i);
        } else {
            printf("Arduino %d: NOT CONNECTED\n", i);
        }
    }

    if(serials[0] != nullptr) {
        printf("Testing arduino 0\n");
        if(!skip()) {
            auto ccenc1 = LLDCEncoder(*serials[0], 0, false);
            auto ccenc2 = LLDCEncoder(*serials[0], 1, true);
            auto but1 = LLButton(*serials[0], 0);
            auto but2 = LLButton(*serials[0], 1);
            auto but3 = LLButton(*serials[0], 2);
            check(ccenc1);
            check(ccenc2);
            check(but1);
            check(but2);
            check(but3);
        } else {
            printf("> Skipped\n");
        }
    }

    if(serials[1] != nullptr) {
        printf("Testing arduino 1\n");
        if(!skip()) {
            auto ccenc = LLDCEncoder(*serials[1], 0, false);
            auto dist1 = LLDistSensor(*serials[1], 0);
            auto dist2 = LLDistSensor(*serials[1], 1);
            auto servo1 = LLBasicServo(*serials[1], 0);
            auto servo2 = LLBasicServo(*serials[1], 1);
            auto servo3 = LLBasicServo(*serials[1], 2);
            auto servo4 = LLBasicServo(*serials[1], 3);
            check(ccenc);
            check(dist1);
            check(dist2);
            check(servo1);
            check(servo2);
            check(servo3);
            check(servo4);
        } else {
            printf("> Skipped\n");
        }
    }

    if(serials[2] != nullptr) {
        printf("Testing arduino 2\n");
        if(!skip()) {
            auto servo1 = LLBasicServo(*serials[2], 0);
            auto servo2 = LLBasicServo(*serials[2], 1);
            auto servo3 = LLBasicServo(*serials[2], 2);
            auto servo4 = LLBasicServo(*serials[2], 3);
            auto but1 = LLButton(*serials[2], 0);
            auto but2 = LLButton(*serials[2], 1);
            auto but3 = LLButton(*serials[2], 2);
            auto ns = LLNiceServo(*serials[2], 0);
            check(ns);
            check(servo1);
            check(servo2);
            check(servo3);
            check(servo4);
            check(but1);
            check(but2);
            check(but3);
        } else {
            printf("> Skipped\n");
        }
    }

#else

    if(serials[0] != nullptr) {
        printf("Testing arduino 0\n");
        if(!skip()) {
            auto ccenc1 = LLDCEncoder(*serials[0], 0, false);
            auto ccenc2 = LLDCEncoder(*serials[0], 1, true);
            auto but1 = LLButton(*serials[0], 0);
            auto dist1 = LLDistSensor(*serials[0], 0);
            check(ccenc1);
            check(ccenc2);
            check(but1);
            check(dist1);
        } else {
            printf("> Skipped\n");
        }
    }



#endif

    disable_non_canonical();
}
