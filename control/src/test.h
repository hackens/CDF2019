#pragma once

#include "llcomp.h"
class Serial;

void check(LLDCEncoder& m);
void check(LLDistSensor& ds);
void check(LLBasicServo& bs);
void check(LLButton& b);
void selfcheck(Serial** serials);
