
#include <iostream>
#include <cassert>
#include <unistd.h>
#include <signal.h>
#include "TGUI/TGUI.hpp"
#include "boost/iostreams/device/file_descriptor.hpp"
#include "boost/iostreams/stream.hpp"

namespace ios = boost::iostreams;

const int windowW = 480;
const int windowH = 320;

static tgui::Gui* g_gui;
static tgui::Button::Ptr g_left, g_right, g_stop;
static tgui::Label::Ptr g_score;
static bool g_inScore;
static std::istream* g_stream;
static std::string g_program;
static pid_t g_child;

std::istream* spawn_process(const std::string& exec,
                            const std::string& arg) {
    pid_t result;
    int fds[2];
    
    assert(pipe(fds) == 0);

    result = fork();
    if(result == 0) {
        /* Child */
        close(fds[0]);
        dup2(fds[1], 1 /* stdout */);
        char* args[3];
        args[0] = new char[exec.size() + 1];
        strcpy(args[0], exec.c_str());
        args[1] = new char[exec.size() + 1];
        strcpy(args[1], arg.c_str());
        args[2] = nullptr;
        execv(args[0], args);
        exit(0);

    } else {
        /* Parent */
        close(fds[1]);
        ios::file_descriptor_source src(fds[0], ios::close_handle);
        ios::stream<ios::file_descriptor_source>* stream =
            new ios::stream<ios::file_descriptor_source>(src);
        g_child = result;
        return stream;
    }
}

void setScore(uint16_t score) {
    std::ostringstream oss;
    oss << "Scr: " << score;
    g_score->setText(oss.str());
}

void setupLeftRightView() {
    g_gui->removeAllWidgets();
    g_gui->add(g_left); g_gui->add(g_right);
    g_left->setPosition("5%", "5%");
    g_left->setSize("40%", "90%");
    g_right->setPosition("55%", "5%");
    g_right->setSize("40%", "90%");
}

void setupStopView() {
    g_gui->removeAllWidgets();
    g_gui->add(g_stop); g_gui->add(g_score);
    setScore(0);
    g_score->setPosition("5%", "5%");
    g_score->setSize("90%", "40%");
    g_stop->setPosition("5%", "55%");
    g_stop->setSize("90%", "40%");
}

void left() {
    g_stream = spawn_process(g_program, "left");
    setupStopView();
    g_inScore = true;
}

void right() {
    g_stream = spawn_process(g_program, "right");
    setupStopView();
    g_inScore = true;
}

void terminate() {
    g_inScore = false;
    kill(g_child, SIGKILL);
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    if(argc != 2) {
        std::cerr << "Usage: " << argv[0] << " path/to/control" << std::endl;
        return 1;
    }
    g_program = argv[1];

    sf::RenderWindow window({windowW,windowH}, "Window", sf::Style::Fullscreen);

    g_gui = new tgui::Gui(window);

    g_left = tgui::Button::create("Left");
    g_left->connect("pressed", left);

    g_right = tgui::Button::create("Right");
    g_right->connect("pressed", right);

    g_stop = tgui::Button::create("Stop");
    g_stop->connect("pressed", terminate);

    g_score = tgui::Label::create("Scr :");
    g_score->setAutoSize(false);
    g_score->setTextSize(130);

    g_inScore = false;
    setupLeftRightView();

    while(window.isOpen()) {
        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed)
                window.close();

            /* Touchscreen is flipped */
            if(event.type == sf::Event::MouseButtonPressed
                    || event.type == sf::Event::MouseButtonReleased) {
                event.mouseButton.x = windowW - event.mouseButton.x;
                event.mouseButton.y = windowH - event.mouseButton.y;
            } else if(event.type == sf::Event::MouseMoved) {
                event.mouseMove.x = windowW - event.mouseMove.x;
                event.mouseMove.y = windowH - event.mouseMove.y;
            }

            g_gui->handleEvent(event);
        }

        window.clear(sf::Color(255,0,0,255));
        g_gui->draw();
        window.display();

        if(g_inScore) {
            uint32_t score;
            (*g_stream) >> score;
            setScore(score);
        }
    }

    return 0;
}

