{ pkgs ? import <nixpkgs> {} }:
rec {
  cdf19-arduino = pkgs.stdenv.mkDerivation {
    name = "cdf19-gui";
    version = "0.1";
    nativeBuildInputs = with pkgs; [
    ];
    buildInputs = with pkgs; [
      gcc cmake sfml boost
    ];
  };
}

