from math import sin, cos, pi
import turtle

sc = 3.5
tx = lambda x: (x - 1000)/sc
ty = lambda y: (y - 750)/sc

relative = True
ref = None

class pos():
    def __init__(self, d, x, y):
        self.x = x
        self.y = y
        self.d = d
    def __repr__(self):
        if relative:
            ax = self.x - ref.x
            ay = self.y - ref.y
            aa = (self.d - ref.d) % 360
            rx = ax * c(ref.d-90) + ay * s(ref.d-90)
            ry = (-ax * s(ref.d-90)) + ay * c(ref.d-90)
            return "pos({}°, {} mm, {} mm)".format(aa, rx, ry)    
        return "pos({}°, {} mm, {} mm)".format(self.d, self.x, self.y)
    def __str__(self):
        if relative:
            ax = self.x - ref.x
            ay = self.y - ref.y
            aa = (self.d - ref.d) % 360
            rx = ax * c(ref.d-90) + ay * s(ref.d-90)
            ry = (ax * s(ref.d-90)) + ay * c(ref.d-90)
            return "({}°, {}, {})".format(round(aa), round(rx), round(ry))    
        return "({}, {}, {})".format(round(self.d), round(self.x), round(self.y))
    def __sub__(self, other):
        return pos(self.d, self.x - other.x, self.y - other.y)

c = lambda x: cos(x*pi/180)
s = lambda x: sin(x*pi/180)

def draw(a,r):
    turtle.penup()
    turtle.setpos(tx(a.x), ty(a.y))
    turtle.pendown()
    turtle.setpos(tx(r.x), ty(r.y))

def av(p, D):
    r = pos(p.d, p.x+D*s(p.d), p.y+D*c(p.d))
    print("Avant {}".format(D), r, sep = "\t\t\t")
    draw(p,r)
    return r
def ar(p, D):
    r = pos(p.d, p.x - D*s(p.d), p.y - D*c(p.d))
    print("Arriere {}".format(D), r, sep = "\t\t\t")
    draw(p,r)
    return r

srb_av = 150
srb_wd = 255
srb_bc = -110

brb_rd = 150

def smallbot(a):
    turtle.penup()
    color = turtle.color()
    px = s(a.d)
    py = c(a.d)
    turtle.setpos(tx(a.x + srb_av*px), ty(a.y+srb_av*py))
    turtle.color("red", "#ff5555")
    turtle.begin_fill()
    turtle.pendown()
    turtle.setpos(tx(a.x + srb_av*px - srb_wd*py), ty(a.y+srb_av*py + srb_wd*px))
    turtle.setpos(tx(a.x + srb_bc*px - srb_wd*py), ty(a.y+srb_bc*py + srb_wd*px))
    turtle.setpos(tx(a.x + srb_bc*px), ty(a.y+srb_bc*py))
    turtle.setpos(tx(a.x + srb_av*px), ty(a.y+srb_av*py))
    turtle.end_fill()
    turtle.color(*color)

def palets(a):
    turtle.penup()
    color = turtle.color()
    px = s(a.d)
    py = c(a.d)
    turtle.setpos(tx(a.x + srb_av*px), ty(a.y+srb_av*py))
    turtle.color("blue", "#5555ff")
    turtle.begin_fill()
    turtle.pendown()
    turtle.setpos(tx(a.x + srb_av*px - srb_wd*py), ty(a.y+srb_av*py + srb_wd*px))
    turtle.setpos(tx(a.x - srb_wd*py), ty(a.y + srb_wd*px))
    turtle.setpos(tx(a.x), ty(a.y))
    turtle.setpos(tx(a.x + srb_av*px), ty(a.y+srb_av*py))
    turtle.end_fill()
    turtle.color(*color)
    
# petit robot
def rot(p, d, rev = False):
    ss = "\t"
    if rev:
        ss = " (rev)"
    r = pos(d%360, p.x, p.y)
    print("rot {} droit{}".format((p.d-d) % 360, ss), r, sep = "\t")
    draw(p,r)
    return r
def rotg(p, ds, rev = False):
    ss = "\t"
    if rev:
        ss = " (rev)"
    r = pos(ds%360, p.x - srb_wd*c(p.d) + srb_wd*c(ds), p.y + srb_wd*s(p.d) - srb_wd*s(ds))
    print("rot {} gauche".format((p.d-ds) % 360, ss), r, sep = "\t")
    turtle.pencolor("brown")
    draw(p,r)
    turtle.pencolor("blue")
    return r

turtle.speed(0)
def rectangle(x,y,x2, y2):
    turtle.penup()
    turtle.setpos(tx(x), ty(y))
    turtle.pendown()
    turtle.setpos(tx(x), ty(y2))
    turtle.setpos(tx(x2), ty(y2))
    turtle.setpos(tx(x2), ty(y))
    turtle.setpos(tx(x), ty(y))

def bigbot(a):
    turtle.penup()
    color = turtle.color()
    turtle.setpos(tx(a.x), ty(a.y-brb_rd))
    turtle.color("green", "#55ff55")
    turtle.begin_fill()
    turtle.circle(brb_rd/sc)
    turtle.end_fill()
    turtle.color(*color)

def circle(x,y,r):
    turtle.penup()
    turtle.setpos(tx(x), ty(y-r))
    turtle.pendown()
    turtle.circle(r/sc)    

def map():
    rectangle(0,0,2000,1500)
    rectangle(300, 0, 600, 450)
    rectangle(600, 0, 900, 450)
    rectangle(900, 0, 1200, 450)
    rectangle(1600, 450, 2000, 1500)
    circle(1050, 1000, 150)

    circle(450,500,38)
    circle(750,500,38)
    circle(1050,500,38)
    #rectangle(1050-150, 1000-150, 1050+150, 1000+150)
    rectangle(1600,1228,2000,1500)
    rectangle(1543,450,1600,1100)

def small_path():
    turtle.pencolor("blue")
    p0 = pos(0, 1200, -srb_bc)
    print("Start :", p0)
    #smallbot(p0)
    a = rot(p0, 22)
    a = av(a, 710)
    #smallbot(a)
    a = rotg(a, 270)
    print("//palet cercle")
    a = av(a, 370+srb_av+10)
    palets(a)
    print("//laches")
    a = ar(a, srb_av+10)
    a = rot(a, 176, True)
    #smallbot(a)
    a = av(a, 610)
    print("//palet vert")
    a = rot(a, 230)
    a = av(a, 240)
    palets(a)
    print("//laches")
    a = ar(a, 370)
    a = rot(a, 270)
    #smallbot(a)
    print("//palet rouge")
    a = av(a, 400)
    a = rotg(a, 180)
    palets(a)
    print("//laches")
    a = ar(a, srb_av+10)
    a = rot(a, 0, True)
    a = av(a, 370)
    a = rot(a, 90)
    #smallbot(a)

    print("//palet cercle")
    a = av(a, 400)
    a = rot(a, 165)
    a = av(a, 600)
    #smallbot(a)
    a = rotg(a, 90)
    a = av(a, 450)
    #smallbot(a)
    a = rotg(a, 5)
    print("//palet rampe")
    #smallbot(a)
    a = av(a, 900)
    palets(a)

def small_path1():
    # palet tableau 2 rouge bien, 1 vert faux = 1+1+1+5+5 = 13 pts
    # palet cercle (2r,1b,1v) + rampe (1v +? 1b) 4+4+8+8+12+12 = 48 pts
    global relative
    relative = False
    turtle.pencolor("blue")
    p0 = pos(0, 1200, -srb_bc)
    print("Start :", p0)
    #smallbot(p0)
    a = rot(p0, 22)
    a = av(a, 710)
    #smallbot(a)
    a = rotg(a, 270)
    print("//palet cercle")
    a = av(a, 370+srb_av+10)
    palets(a)
    print("//laches")
    a = ar(a, srb_av+10)
    a = rot(a, 176, True)
    #smallbot(a)
    a = av(a, 540)
    print("//palet vert")
    #a = rot(a, 230)
    #a = av(a, 240)
    palets(a)
    print("//laches")
    #a = ar(a, 370)
    a = rot(a, 270)
    #smallbot(a)
    print("//palet rouge")
    a = av(a, 350)
    a = rotg(a, 180)
    palets(a)
    print("//laches")
    a = ar(a, srb_av+10)
    a = rot(a, 0, True)
    a = av(a, 370)
    a = rot(a, 90)
    #smallbot(a)

    print("//palet cercle")
    a = av(a, 400)
    a = rot(a, 165)
    a = av(a, 600)
    #smallbot(a)
    a = rotg(a, 90)
    a = av(a, 520)
    #smallbot(a)
    a = rotg(a, 2)
    print("//palet rampe")
    smallbot(a)
    a = av(a, 900)
    palets(a)
    
def big_path():
    p1 = pos(90, 600, brb_rd)
    print("Start :", p1)
    turtle.pencolor("green")
    #bigbot(p1)
    a = av(p1, 943-brb_rd)
    a = rot(a, 0)
    a = av(a, 1050)
    #bigbot(a)
    a = ar(a, 700)
    a = rot(a, 270)
    a = av(a, 943-brb_rd)
    bigbot(a)

def big_basic0():
    p1 = pos(45, 900+150, brb_rd)
    print("Start :", p1)
    turtle.pencolor("green")
    #bigbot(p1)
    a = av(p1, 600)
    a = rot(a, 90)
    a = ar(a, 900)
    #bigbot(a)
    #a = ar(a, 700)
    #a = rot(a, 270)
    #a = av(a, 943-brb_rd)
    bigbot(a)

def big_basic1():
    # 13 points
    global ref
    p1 = pos(90, 900+150, brb_rd)
    ref = p1
    print("Start :", p1)
    turtle.pencolor("green")
    #bigbot(p1)
    a = av(p1, 300)
    a = rot(a, 0)
    a = av(a, 450)
    bigbot(a)
    a = rot(a, 90)
    a = ar(a, 900)
    a = rot(a, 0)
    a = ar(a, 200)
    a = av(a, 400)
    #bigbot(a)
    #a = ar(a, 700)
    #a = rot(a, 270)
    #a = av(a, 943-brb_rd)
    bigbot(a)

map()
#big_basic1()
small_path1()
#big_path()
#input()
